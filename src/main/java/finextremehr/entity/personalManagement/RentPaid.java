/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.personalManagement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author erycoking
 */
@Entity
//@Table(name = "RentPaid")
public class RentPaid {
    
    @Id
    @Column(name = "PayrollNo", nullable = false, length = 20)
    private String PayrollNo;
    
    @Column(name = "Amount")
    private double Amount;

    public RentPaid() {
    }

    public RentPaid(String PayrollNo, double Amount) {
        this.PayrollNo = PayrollNo;
        this.Amount = Amount;
    }

      

    public String getPayrollNo() {
        return PayrollNo;
    }

    public void setPayrollNo(String PayrollNo) {
        this.PayrollNo = PayrollNo;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double Amount) {
        this.Amount = Amount;
    }

    

    @Override
    public String toString() {
        return "RentPaid{" + "PayrollNo=" + PayrollNo + ", Amount=" + Amount + '}';
    }
    
    
    
}
