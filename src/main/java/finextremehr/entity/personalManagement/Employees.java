/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.personalManagement;

import finextremehr.entity.parameters.StaffCategories;
import finextremehr.entity.company.Bank;
import finextremehr.entity.company.Branch;
import finextremehr.entity.company.Department;
import finextremehr.entity.company.Jobgroups;
import finextremehr.entity.company.Sections;
import finextremehr.enums.MaritalStatus;
import finextremehr.enums.PaymentMode;
import finextremehr.enums.PensionDeduction;

import finextremehr.enums.ProvidentMode;
import finextremehr.enums.SEX;
import finextremehr.enums.Status;
import finextremehr.enums.YesNo;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author erycoking
 */
@Entity
@Table(name = "employees")
public class Employees implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "PayrollNo", nullable = false, length = 30, unique = true)
    private String PayrollNo;
    
    @Column(name = "IDNo", nullable = false, length = 30, unique = true)
    private String IDNo;
    
    @Column(name = "BankAccountNumber", nullable = true, length = 40, unique = true)
    private String BankAccountNumber;
    
    @Column(name = "Surname", nullable = false, length = 40)
    private String Surname;
    
    @Column(name = "OtherName", nullable = false, length = 40)
    private String OtherName;
    
    @Column(name = "EmployeeDate", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date EmploymentDate;
    
    @Column(name = "IsHoused", columnDefinition = "enum('YES', 'NO') NOT NULL DEFAULT 'NO'")

    private YesNo IsHoused;
    
    @Column(name = "RetirementAge", nullable = true, length = 11)
    private int RetirementAge;
    
    @Column(name = "DOB", nullable = true, length = 40)
    @Temporal(TemporalType.DATE)
    private Date DOB;
    
    @Column(name = "DeductPensionSelfAs", nullable = false, columnDefinition = "enum('Amount','PercGross','PercBasic')")
    @Enumerated(EnumType.STRING)
    private PensionDeduction DeductPensionSelfAs;
    
    @Column(name = "DeductPensionCompanyAs", nullable = false, columnDefinition = "enum('Amount','PercGross','PercBasic')")
    @Enumerated(EnumType.STRING)
    private PensionDeduction DeductPensionCompanyAs;
    
    @Column(name = "Address", nullable = false, length = 40)
    private String Address;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "staffType", nullable = false)
    private StaffCategories StaffType;
    
    @Column(name = "sex", nullable = false, columnDefinition = "enum('MALE', 'FEMALE')")
    @Enumerated(EnumType.STRING)
    private SEX sex;
    
    @Column(name = "NSSF_No", nullable = true, length = 30, unique = true)
    private String NSSF_No;
    
    @Column(name = "NHIF_No", nullable = true, length = 30, unique = true)
    private String NHIF_No;
    
    @Column(name = "PensionNo", nullable = true, length = 30, unique = true)
    private String PensionNo;
    
    @Column(name = "CoopNo", nullable = true, length = 30, unique = true)
    private String CoopNo;
    
    @Column(name = "Mode_Of_Pay", nullable = false, columnDefinition = "enum('CASH','BANK','CHEQUE')")
    @Enumerated(EnumType.STRING)
    private PaymentMode Mode_Of_Pay;
    

    @Column(name = "Pension", nullable = true, columnDefinition = "enum('YES','NO') NOT NULL")
    private YesNo Pension;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "BankCode")
    private Bank BankCode;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "BranchCode")
    private Branch BranchCode;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "JobGroupCode")
    private Jobgroups JobGroupCode;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "DepartmentCode")
    private Department DepartmentCode;
    
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "SectionCode")
    private Sections SectionCode;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "MaritalStatus", nullable = false, columnDefinition = "enum('Married','Not_Married')")
    private MaritalStatus MaritalStatus;
    

    @Column(name = "ExemptedFromNSSF", columnDefinition = "enum('YES','NO') NOT NULL")
    private YesNo ExemptedFromNSSF;
    
  
    @Column(name = "ExemptedFromNHIF", columnDefinition = "enum('YES','NO') NOT NULL")
    private YesNo ExemptedFromNHIF;
    
    @Column(name = "PensionSelfBF", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double PensionSelfBF;
    
    @Column(name = "PensionCompanyBF", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double PensionCompanyBF;
    
    @Column(name = "PensionSelf", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double PensionSelf;
    
    @Column(name = "PensionCompany", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double PensionCompany;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "ProvidentSelfMode", nullable = false, columnDefinition ="enum('BASIC','GROSS','AMOUNT') default 'AMOUNT'")
    private ProvidentMode ProvidentSelfMode;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "ProvidentCompanyMode", nullable = false, columnDefinition ="enum('BASIC','GROSS','AMOUNT') default 'AMOUNT'")
    private ProvidentMode ProvidentCompanyMode;
    
   
    @Column(name = "Provident", columnDefinition ="enum('YES', 'NO') NOT NULL default 'NO'")
    private YesNo Provident;
    
    @Column(name = "ProvidentSelfBF", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double ProvidentSelfBF;
    
    @Column(name = "ProvidentCompanyBF", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double ProvidentCompanyBF;
    
    @Column(name = "ProvidentSelf", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double ProvidentSelf;
    
    @Column(name = "ProvidentCompany", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double ProvidentCompany;
    
    @Column(name = "EmployeePIN", nullable = true, length = 20)
    private String EmployeePIN;
    
    @Column(name = "NSSFSelf", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double NSSFSelf;
    
    @Column(name = "NSSFSelfBF", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double NSSFSelfBF;
    
    @Column(name = "NSSFCompany", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double NSSFCompany;
    
    @Column(name = "NSSFCompanyBF", nullable = false, columnDefinition ="Decimal(15,2) default '0.00'")
    private double NSSFCompanyBF;
    
    @Column(name = "Status", nullable = false, columnDefinition ="enum('ACTIVE','TERM','SUSPE') default 'ACTIVE'")
    @Enumerated(EnumType.STRING)
    private Status Status;
    
    @Column(name = "EmpPass", nullable = true, length = 100)
    private String EmpPass;
    
    @Column(name = "BloodGroup", nullable = true, length = 11)
    private int BloodGroup;
    
    @Column(name = "CellPhone", nullable = true, length = 20)
    private String CellPhone;
    
    @Column(name = "EmailAddress", nullable = true, length = 30)
    private String EmailAddress;

    public Employees() {
    }

    public Employees(String PayrollNo, String IDNo, String BankAccountNumber, String Surname, String OtherName, Date EmploymentDate, YesNo IsHoused, int RetirementAge, Date DOB, PensionDeduction DeductPensionSelfAs, PensionDeduction DeductPensionCompanyAs, String Address, StaffCategories StaffType, SEX sex, String NSSF_No, String NHIF_No, String PensionNo, String CoopNo, PaymentMode Mode_Of_Pay, YesNo Pension, Bank BankCode, Branch BranchCode, Jobgroups JobGroupCode, Department DepartmentCode, Sections SectionCode, MaritalStatus MaritalStatus, YesNo ExemptedFromNSSF, YesNo ExemptedFromNHIF, double PensionSelfBF, double PensionCompanyBF, double PensionSelf, double PensionCompany, ProvidentMode ProvidentSelfMode, ProvidentMode ProvidentCompanyMode, YesNo Provident, double ProvidentSelfBF, double ProvidentCompanyBF, double ProvidentSelf, double ProvidentCompany, String EmployeePIN, double NSSFSelf, double NSSFSelfBF, double NSSFCompany, double NSSFCompanyBF, Status Status, String EmpPass, int BloodGroup, String CellPhone, String EmailAddress) {
        this.PayrollNo = PayrollNo;
        this.IDNo = IDNo;
        this.BankAccountNumber = BankAccountNumber;
        this.Surname = Surname;
        this.OtherName = OtherName;
        this.EmploymentDate = EmploymentDate;
        this.IsHoused = IsHoused;
        this.RetirementAge = RetirementAge;
        this.DOB = DOB;
        this.DeductPensionSelfAs = DeductPensionSelfAs;
        this.DeductPensionCompanyAs = DeductPensionCompanyAs;
        this.Address = Address;
        this.StaffType = StaffType;
        this.sex = sex;
        this.NSSF_No = NSSF_No;
        this.NHIF_No = NHIF_No;
        this.PensionNo = PensionNo;
        this.CoopNo = CoopNo;
        this.Mode_Of_Pay = Mode_Of_Pay;
        this.Pension = Pension;
        this.BankCode = BankCode;
        this.BranchCode = BranchCode;
        this.JobGroupCode = JobGroupCode;
        this.DepartmentCode = DepartmentCode;
        this.SectionCode = SectionCode;
        this.MaritalStatus = MaritalStatus;
        this.ExemptedFromNSSF = ExemptedFromNSSF;
        this.ExemptedFromNHIF = ExemptedFromNHIF;
        this.PensionSelfBF = PensionSelfBF;
        this.PensionCompanyBF = PensionCompanyBF;
        this.PensionSelf = PensionSelf;
        this.PensionCompany = PensionCompany;
        this.ProvidentSelfMode = ProvidentSelfMode;
        this.ProvidentCompanyMode = ProvidentCompanyMode;
        this.Provident = Provident;
        this.ProvidentSelfBF = ProvidentSelfBF;
        this.ProvidentCompanyBF = ProvidentCompanyBF;
        this.ProvidentSelf = ProvidentSelf;
        this.ProvidentCompany = ProvidentCompany;
        this.EmployeePIN = EmployeePIN;
        this.NSSFSelf = NSSFSelf;
        this.NSSFSelfBF = NSSFSelfBF;
        this.NSSFCompany = NSSFCompany;
        this.NSSFCompanyBF = NSSFCompanyBF;
        this.Status = Status;
        this.EmpPass = EmpPass;
        this.BloodGroup = BloodGroup;
        this.CellPhone = CellPhone;
        this.EmailAddress = EmailAddress;
    }

    public String getPayrollNo() {
        return PayrollNo;
    }

    public void setPayrollNo(String PayrollNo) {
        this.PayrollNo = PayrollNo;
    }

    public String getIDNo() {
        return IDNo;
    }

    public void setIDNo(String IDNo) {
        this.IDNo = IDNo;
    }

    public String getBankAccountNumber() {
        return BankAccountNumber;
    }

    public void setBankAccountNumber(String BankAccountNumber) {
        this.BankAccountNumber = BankAccountNumber;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String Surname) {
        this.Surname = Surname;
    }

    public String getOtherName() {
        return OtherName;
    }

    public void setOtherName(String OtherName) {
        this.OtherName = OtherName;
    }

    public Date getEmploymentDate() {
        return EmploymentDate;
    }

    public void setEmploymentDate(Date EmploymentDate) {
        this.EmploymentDate = EmploymentDate;
    }

    public YesNo getIsHoused() {
        return IsHoused;
    }

    public void setIsHoused(YesNo IsHoused) {
        this.IsHoused = IsHoused;
    }

    public int getRetirementAge() {
        return RetirementAge;
    }

    public void setRetirementAge(int RetirementAge) {
        this.RetirementAge = RetirementAge;
    }

    public Date getDOB() {
        return DOB;
    }

    public void setDOB(Date DOB) {
        this.DOB = DOB;
    }

    public PensionDeduction getDeductPensionSelfAs() {
        return DeductPensionSelfAs;
    }

    public void setDeductPensionSelfAs(PensionDeduction DeductPensionSelfAs) {
        this.DeductPensionSelfAs = DeductPensionSelfAs;
    }

    public PensionDeduction getDeductPensionCompanyAs() {
        return DeductPensionCompanyAs;
    }

    public void setDeductPensionCompanyAs(PensionDeduction DeductPensionCompanyAs) {
        this.DeductPensionCompanyAs = DeductPensionCompanyAs;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public StaffCategories getStaffType() {
        return StaffType;
    }

    public void setStaffType(StaffCategories StaffType) {
        this.StaffType = StaffType;
    }

    public SEX getSex() {
        return sex;
    }

    public void setSex(SEX sex) {
        this.sex = sex;
    }

    public String getNSSF_No() {
        return NSSF_No;
    }

    public void setNSSF_No(String NSSF_No) {
        this.NSSF_No = NSSF_No;
    }

    public String getNHIF_No() {
        return NHIF_No;
    }

    public void setNHIF_No(String NHIF_No) {
        this.NHIF_No = NHIF_No;
    }

    public String getPensionNo() {
        return PensionNo;
    }

    public void setPensionNo(String PensionNo) {
        this.PensionNo = PensionNo;
    }

    public String getCoopNo() {
        return CoopNo;
    }

    public void setCoopNo(String CoopNo) {
        this.CoopNo = CoopNo;
    }

    public PaymentMode getMode_Of_Pay() {
        return Mode_Of_Pay;
    }

    public void setMode_Of_Pay(PaymentMode Mode_Of_Pay) {
        this.Mode_Of_Pay = Mode_Of_Pay;
    }

    public YesNo getPension() {
        return Pension;
    }

    public void setPension(YesNo Pension) {
        this.Pension = Pension;
    }

    public Bank getBankCode() {
        return BankCode;
    }

    public void setBankCode(Bank BankCode) {
        this.BankCode = BankCode;
    }

    public Branch getBranchCode() {
        return BranchCode;
    }

    public void setBranchCode(Branch BranchCode) {
        this.BranchCode = BranchCode;
    }

    public Jobgroups getJobGroupCode() {
        return JobGroupCode;
    }

    public void setJobGroupCode(Jobgroups JobGroupCode) {
        this.JobGroupCode = JobGroupCode;
    }

    public Department getDepartmentCode() {
        return DepartmentCode;
    }

    public void setDepartmentCode(Department DepartmentCode) {
        this.DepartmentCode = DepartmentCode;
    }

    public Sections getSectionCode() {
        return SectionCode;
    }

    public void setSectionCode(Sections SectionCode) {
        this.SectionCode = SectionCode;
    }

    public MaritalStatus getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(MaritalStatus MaritalStatus) {
        this.MaritalStatus = MaritalStatus;
    }

    public YesNo getExemptedFromNSSF() {
        return ExemptedFromNSSF;
    }

    public void setExemptedFromNSSF(YesNo ExemptedFromNSSF) {
        this.ExemptedFromNSSF = ExemptedFromNSSF;
    }

    public YesNo getExemptedFromNHIF() {
        return ExemptedFromNHIF;
    }

    public void setExemptedFromNHIF(YesNo ExemptedFromNHIF) {
        this.ExemptedFromNHIF = ExemptedFromNHIF;
    }

    public double getPensionSelfBF() {
        return PensionSelfBF;
    }

    public void setPensionSelfBF(double PensionSelfBF) {
        this.PensionSelfBF = PensionSelfBF;
    }

    public double getPensionCompanyBF() {
        return PensionCompanyBF;
    }

    public void setPensionCompanyBF(double PensionCompanyBF) {
        this.PensionCompanyBF = PensionCompanyBF;
    }

    public double getPensionSelf() {
        return PensionSelf;
    }

    public void setPensionSelf(double PensionSelf) {
        this.PensionSelf = PensionSelf;
    }

    public double getPensionCompany() {
        return PensionCompany;
    }

    public void setPensionCompany(double PensionCompany) {
        this.PensionCompany = PensionCompany;
    }

    public ProvidentMode getProvidentSelfMode() {
        return ProvidentSelfMode;
    }

    public void setProvidentSelfMode(ProvidentMode ProvidentSelfMode) {
        this.ProvidentSelfMode = ProvidentSelfMode;
    }

    public ProvidentMode getProvidentCompanyMode() {
        return ProvidentCompanyMode;
    }

    public void setProvidentCompanyMode(ProvidentMode ProvidentCompanyMode) {
        this.ProvidentCompanyMode = ProvidentCompanyMode;
    }

    public YesNo getProvident() {
        return Provident;
    }

    public void setProvident(YesNo Provident) {
        this.Provident = Provident;
    }

   

    public double getProvidentSelfBF() {
        return ProvidentSelfBF;
    }

    public void setProvidentSelfBF(double ProvidentSelfBF) {
        this.ProvidentSelfBF = ProvidentSelfBF;
    }

    public double getProvidentCompanyBF() {
        return ProvidentCompanyBF;
    }

    public void setProvidentCompanyBF(double ProvidentCompanyBF) {
        this.ProvidentCompanyBF = ProvidentCompanyBF;
    }

    public double getProvidentSelf() {
        return ProvidentSelf;
    }

    public void setProvidentSelf(double ProvidentSelf) {
        this.ProvidentSelf = ProvidentSelf;
    }

    public double getProvidentCompany() {
        return ProvidentCompany;
    }

    public void setProvidentCompany(double ProvidentCompany) {
        this.ProvidentCompany = ProvidentCompany;
    }

    public String getEmployeePIN() {
        return EmployeePIN;
    }

    public void setEmployeePIN(String EmployeePIN) {
        this.EmployeePIN = EmployeePIN;
    }

    public double getNSSFSelf() {
        return NSSFSelf;
    }

    public void setNSSFSelf(double NSSFSelf) {
        this.NSSFSelf = NSSFSelf;
    }

    public double getNSSFSelfBF() {
        return NSSFSelfBF;
    }

    public void setNSSFSelfBF(double NSSFSelfBF) {
        this.NSSFSelfBF = NSSFSelfBF;
    }

    public double getNSSFCompany() {
        return NSSFCompany;
    }

    public void setNSSFCompany(double NSSFCompany) {
        this.NSSFCompany = NSSFCompany;
    }

    public double getNSSFCompanyBF() {
        return NSSFCompanyBF;
    }

    public void setNSSFCompanyBF(double NSSFCompanyBF) {
        this.NSSFCompanyBF = NSSFCompanyBF;
    }

    public Status getStatus() {
        return Status;
    }

    public void setStatus(Status Status) {
        this.Status = Status;
    }

    public String getEmpPass() {
        return EmpPass;
    }

    public void setEmpPass(String EmpPass) {
        this.EmpPass = EmpPass;
    }

    public int getBloodGroup() {
        return BloodGroup;
    }

    public void setBloodGroup(int BloodGroup) {
        this.BloodGroup = BloodGroup;
    }

    public String getCellPhone() {
        return CellPhone;
    }

    public void setCellPhone(String CellPhone) {
        this.CellPhone = CellPhone;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String EmailAddress) {
        this.EmailAddress = EmailAddress;
    } 
    
}
