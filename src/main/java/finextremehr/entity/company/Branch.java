/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.company;



import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import javax.persistence.Table;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "branches")
@NamedQueries({
    @NamedQuery(name = "Branch.findAll", query = "SELECT b FROM Branch b")})
public class Branch implements Serializable {

    

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "BranchCode",length=20,nullable=false)
    private String branchCode;
    
    @Basic(optional = false)    
    @Column(name = "BranchName",length=20,nullable=false, unique = true)
    private String branchName;
    
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="bank",nullable=false)
    private Bank bank;

    public Branch() {
    }

    public Branch(String branchCode) {
        this.branchCode = branchCode;
    }

    
    public Branch(String branchCode, String branchName, Bank bank) {
        this.branchCode = branchCode;
        this.branchName = branchName;
        this.bank = bank;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (branchCode != null ? branchCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Branch)) {
            return false;
        }
        Branch other = (Branch) object;
        if ((this.branchCode == null && other.branchCode != null) || (this.branchCode != null && !this.branchCode.equals(other.branchCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fineXtreme.entity.Branches[ branchCode=" + branchCode + " ]";
    }

   


    
}
