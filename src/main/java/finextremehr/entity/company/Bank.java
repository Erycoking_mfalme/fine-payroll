/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.company;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 *
 * @author kadan
 */
@Entity
@Table(name = "banks")
@NamedQueries({
    @NamedQuery(name = "Bank.findAll", query = "SELECT b FROM Bank b")})
public class Bank implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    
    @Column(name = "BankCode",length=20,nullable=false,unique = true)
    private String bankCode;
    
  
    @Column(name = "BankName",nullable=false,length=50,unique = true)
    private String bankName;
   
   @OneToMany(cascade = CascadeType.ALL,orphanRemoval=true,fetch = FetchType.LAZY,mappedBy = "bank")
    private List<Branch> branches;

    public Bank() {
    }

    public Bank(String bankCode) {
        this.bankCode = bankCode;
    }
    
    

    public Bank(String bankCode, String bankName, List<Branch> branch) {
        this.bankCode = bankCode;
        this.bankName = bankName;
        this.branches   = branch;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bankCode != null ? bankCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bank)) {
            return false;
        }
        Bank other = (Bank) object;
        if ((this.bankCode == null && other.bankCode != null) || (this.bankCode != null && !this.bankCode.equals(other.bankCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        //return "finextremehr.entity.Banks[ bankCode=" + bankCode + " ]";
        return String.join("_", this.bankName.split(" ")) + this.bankCode;
    }
    
}
