/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.company;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "benefits")
@NamedQueries({
    @NamedQuery(name = "Benefits.findAll", query = "SELECT b FROM Benefits b")})
public class Benefits implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "BenefitCode")
    private String benefitCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "BenefitName")
    private String benefitName;

    public Benefits() {
    }

    public Benefits(String benefitCode) {
        this.benefitCode = benefitCode;
    }

    public Benefits(String benefitCode, String benefitName) {
        this.benefitCode = benefitCode;
        this.benefitName = benefitName;
    }

    public String getBenefitCode() {
        return benefitCode;
    }

    public void setBenefitCode(String benefitCode) {
        this.benefitCode = benefitCode;
    }

    public String getBenefitName() {
        return benefitName;
    }

    public void setBenefitName(String benefitName) {
        this.benefitName = benefitName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (benefitCode != null ? benefitCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Benefits)) {
            return false;
        }
        Benefits other = (Benefits) object;
        if ((this.benefitCode == null && other.benefitCode != null) || (this.benefitCode != null && !this.benefitCode.equals(other.benefitCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fineXtreme.controller.Benefits[ benefitCode=" + benefitCode + " ]";
    }
    
}
