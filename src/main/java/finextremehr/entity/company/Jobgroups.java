/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.company;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "jobgroups")
@NamedQueries({
    @NamedQuery(name = "Jobgroups.findAll", query = "SELECT j FROM Jobgroups j")})
public class Jobgroups implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "JobGroupCode")
    private String jobGroupCode;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "JobGroupName")
    private String jobGroupName;

    public Jobgroups() {
    }

    public Jobgroups(String jobGroupCode) {
        this.jobGroupCode = jobGroupCode;
    }

    public Jobgroups(String jobGroupCode, String jobGroupName) {
        this.jobGroupCode = jobGroupCode;
        this.jobGroupName = jobGroupName;
    }

    public String getJobGroupCode() {
        return jobGroupCode;
    }

    public void setJobGroupCode(String jobGroupCode) {
        this.jobGroupCode = jobGroupCode;
    }

    public String getJobGroupName() {
        return jobGroupName;
    }

    public void setJobGroupName(String jobGroupName) {
        this.jobGroupName = jobGroupName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (jobGroupCode != null ? jobGroupCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Jobgroups)) {
            return false;
        }
        Jobgroups other = (Jobgroups) object;
        if ((this.jobGroupCode == null && other.jobGroupCode != null) || (this.jobGroupCode != null && !this.jobGroupCode.equals(other.jobGroupCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fineXtreme.controller.Jobgroups[ jobGroupCode=" + jobGroupCode + " ]";
    }
    
}
