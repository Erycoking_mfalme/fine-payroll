/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.company;


import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "sections")
@NamedQueries({
    @NamedQuery(name = "Section.findAll", query = "SELECT s FROM Sections s")})
public class Sections implements Serializable {

    

    private static final long serialVersionUID = 1L;
    @Id
    
    @Column(name = "SectionCode",length=20,nullable=false, unique = true)
    private String sectionCode;
    
    
    @Column(name = "SectionName",length=20,nullable=false, unique = true)
    private String sectionName;
    
    @ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.LAZY)
    @JoinColumn(name="department",nullable=false)
   private Department department;

    public Sections() {
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Sections(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public Sections(String sectionCode, String sectionName) {
        this.sectionCode = sectionCode;
        this.sectionName = sectionName;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sectionCode != null ? sectionCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sections)) {
            return false;
        }
        Sections other = (Sections) object;
        if ((this.sectionCode == null && other.sectionCode != null) || (this.sectionCode != null && !this.sectionCode.equals(other.sectionCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fineXtreme.controller.Sections[ sectionCode=" + sectionCode + " ]";
    }
}
