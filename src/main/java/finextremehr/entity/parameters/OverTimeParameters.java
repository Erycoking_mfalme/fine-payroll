/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 *
 * @author fred
 */
@Entity
@Table(name = "OverTimeParameters")
@NamedQueries({
    @NamedQuery(name = "OverTimeParameters.findAll", query = "SELECT o FROM OverTimeParameters o")})
public class OverTimeParameters implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id    
    @Column(name = "id",nullable=false)
    private Integer id;
    
    @Column(name = "DaysPerMonth",length=11)
    private Integer daysPerMonth;
  
    @Column(name = "HoursPerDay",nullable=false,precision=15,scale=2,columnDefinition="double(15,2)  DEFAULT '0.00'")
    private double hoursPerDay;
   
    @Column(name = "WDFactor",nullable=false,precision=15,scale=5,columnDefinition=" double(15,5)  DEFAULT '1.00000'")
    private double wDFactor;
    
    @Column(name = "NONWDFactor",nullable=false,precision=15,scale=5,columnDefinition=" double(15,5)  DEFAULT '1.00000'")
    private double nONWDFactor;
    
    @JoinColumn(name = "OverTimeCode", referencedColumnName = "EarningCode")
    @ManyToOne
    private GeneralEarnings overTimeCode;

    public OverTimeParameters() {
    }

    public OverTimeParameters(Integer id) {
        this.id = id;
    }

    public OverTimeParameters(Integer id, double hoursPerDay, double wDFactor, double nONWDFactor) {
        this.id = id;
        this.hoursPerDay = hoursPerDay;
        this.wDFactor = wDFactor;
        this.nONWDFactor = nONWDFactor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDaysPerMonth() {
        return daysPerMonth;
    }

    public void setDaysPerMonth(Integer daysPerMonth) {
        this.daysPerMonth = daysPerMonth;
    }

    public double getHoursPerDay() {
        return hoursPerDay;
    }

    public void setHoursPerDay(double hoursPerDay) {
        this.hoursPerDay = hoursPerDay;
    }

    public double getWDFactor() {
        return wDFactor;
    }

    public void setWDFactor(double wDFactor) {
        this.wDFactor = wDFactor;
    }

    public double getNONWDFactor() {
        return nONWDFactor;
    }

    public void setNONWDFactor(double nONWDFactor) {
        this.nONWDFactor = nONWDFactor;
    }

    public GeneralEarnings getOverTimeCode() {
        return overTimeCode;
    }

    public void setOverTimeCode(GeneralEarnings overTimeCode) {
        this.overTimeCode = overTimeCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OverTimeParameters)) {
            return false;
        }
        OverTimeParameters other = (OverTimeParameters) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.parameters.OverTimeParameters[ id=" + id + " ]";
    }
    
}
