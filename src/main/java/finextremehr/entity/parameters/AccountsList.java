/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;


import finextremehr.enums.MemEffect;
import finextremehr.enums.YesNo;
import java.io.Serializable;
import java.sql.Time;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author fred
 */
@Entity
@Table(name = "AccountsList")
@NamedQueries({
    @NamedQuery(name = "AccountsList.findAll", query = "SELECT a FROM AccountsList a")})
public class AccountsList implements Serializable {  

    private static final long serialVersionUID = 1L;
    @Column(name = "LastVal",length=11,columnDefinition=" MEDIUMINT DEFAULT '0'")
    private int lastVal;
    
    @Id       
    @Column(name = "sysCode",length=150,nullable=false)
    private String sysCode;    
    
    @Column(name = "AccountName",length=70,nullable=false)
    private String accountName;
    
    @Column(name = "UserCode",length=25,nullable=false)
    private String userCode;
   
    @Column(name = "AccountCode",length=20)
    private String accountCode;    
   
    @Column(name = "ClassCode",length=20)
    private String classCode;    
   
    @Enumerated(EnumType.STRING)
    @Column(name = "Busy",nullable=false,columnDefinition="enum('YES','NO')  DEFAULT 'NO'")
    private YesNo busy;    
    
    @Enumerated(EnumType.STRING)
    @Column(name = "Active",nullable=false,columnDefinition=" enum('YES','NO') DEFAULT 'YES'")
    private YesNo active;
   
    @Enumerated(EnumType.STRING)
    @Column(name = "Transacting",nullable=false,columnDefinition=" enum('YES','NO') DEFAULT 'NO'")
    private YesNo transacting;    
    
    @Enumerated(EnumType.STRING)
    @Column(name = "MemEffect",nullable=false,columnDefinition="enum('SHARES','LOANS','INTEREST','DRILLD','WELFARE','EQUITY','INTREC','INSURA','PENALTY','NON')  DEFAULT 'NON'")
    private MemEffect memEffect;    
    
    @Column(name = "RptCode",length=10)
    private String rptCode;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "Notified",nullable=false,columnDefinition=" enum('YES','NO') DEFAULT 'NO'")  
    private YesNo notified;    
  
    @Column(name = "Clerk",length=45)
    private String clerk;

    @Temporal(TemporalType.DATE)
    @Column(name = "CreationDate")
    private Date creationDate;
    
    @Temporal(TemporalType.TIME)
    @Column(name = "CreationHour")
    private Date creationHour;    
   
    @Column(name = "LastTrxDate",nullable=false)
    @Temporal(TemporalType.DATE)
    private Date lastTrxDate;
    
    @Column(name = "LastTrxTime",nullable=false)
    @Temporal(TemporalType.TIME)
    private Date lastTrxTime;
    
      @Enumerated(EnumType.STRING)
    @Column(name = "IsControlAc",nullable=false,columnDefinition=" enum('YES','NO') DEFAULT 'NO'") 
    private YesNo isControlAc;
      
    @Enumerated(EnumType.STRING)
    @Column(name = "DirectTellerAccess",nullable=false,columnDefinition=" enum('YES','NO') DEFAULT 'NO'")   
    private YesNo directTellerAccess;
    
     @OneToMany(mappedBy = "xtremeSysCode",cascade=CascadeType.ALL)
    private List<XtremeDeductionInterface> xtremeDeductionInterfaceList;  
     
    @ManyToOne(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
    @JoinColumn(name="BranchCode",referencedColumnName = "BranchCode")
    private SaccoBranches branchCode;
    
    @ManyToOne
    @JoinColumn(name="RptCode",referencedColumnName="RptTitleCode",insertable=false, updatable=false)
    private ReportHeaders reportHeader;
     

    public AccountsList() {
    }

    public AccountsList(String sysCode) {
        this.sysCode = sysCode;
    }

    public AccountsList(int lastVal, String sysCode, String accountName, String userCode, String accountCode, String classCode, YesNo busy, YesNo active, YesNo transacting, MemEffect memEffect, String rptCode, YesNo notified, String clerk, Date creationDate, Date creationHour, Date lastTrxDate, Date lastTrxTime, YesNo isControlAc, YesNo directTellerAccess, List<XtremeDeductionInterface> xtremeDeductionInterfaceList, SaccoBranches branchCode, ReportHeaders reportHeader) {
        this.lastVal = lastVal;
        this.sysCode = sysCode;
        this.accountName = accountName;
        this.userCode = userCode;
        this.accountCode = accountCode;
        this.classCode = classCode;
        this.busy = busy;
        this.active = active;
        this.transacting = transacting;
        this.memEffect = memEffect;
        this.rptCode = rptCode;
        this.notified = notified;
        this.clerk = clerk;
        this.creationDate = creationDate;
        this.creationHour = creationHour;
        this.lastTrxDate = lastTrxDate;
        this.lastTrxTime = lastTrxTime;
        this.isControlAc = isControlAc;
        this.directTellerAccess = directTellerAccess;
        this.xtremeDeductionInterfaceList = xtremeDeductionInterfaceList;
        this.branchCode = branchCode;
        this.reportHeader = reportHeader;
    }

    

    

    public ReportHeaders getReportHeader() {
        return reportHeader;
    }

    public void setReportHeader(ReportHeaders reportHeader) {
        this.reportHeader = reportHeader;
    }

   
    public SaccoBranches getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(SaccoBranches branchCode) {
        this.branchCode = branchCode;
    }
    
    
   

    public Integer getLastVal() {
        return lastVal;
    }

    public void setLastVal(Integer lastVal) {
        this.lastVal = lastVal;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public YesNo getBusy() {
        return busy;
    }

    public void setBusy(YesNo busy) {
        this.busy = busy;
    }

    public YesNo getActive() {
        return active;
    }

    public void setActive(YesNo active) {
        this.active = active;
    }

    public YesNo getTransacting() {
        return transacting;
    }

    public void setTransacting(YesNo transacting) {
        this.transacting = transacting;
    }

    public MemEffect getMemEffect() {
        return memEffect;
    }

    public void setMemEffect(MemEffect memEffect) {
        this.memEffect = memEffect;
    }

    public String getRptCode() {
        return rptCode;
    }

    public void setRptCode(String rptCode) {
        this.rptCode = rptCode;
    }

    public YesNo getNotified() {
        return notified;
    }

    public void setNotified(YesNo notified) {
        this.notified = notified;
    }

    

    public String getClerk() {
        return clerk;
    }

    public void setClerk(String clerk) {
        this.clerk = clerk;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getCreationHour() {
        return creationHour;
    }

    public void setCreationHour(Time creationHour) {
        this.creationHour = creationHour;
    }

    public Date getLastTrxDate() {
        return lastTrxDate;
    }

    public void setLastTrxDate(Date lastTrxDate) {
        this.lastTrxDate = lastTrxDate;
    }

    public Date getLastTrxTime() {
        return lastTrxTime;
    }

    public void setLastTrxTime(Date lastTrxTime) {
        this.lastTrxTime = lastTrxTime;
    }

    public YesNo getIsControlAc() {
        return isControlAc;
    }

    public void setIsControlAc(YesNo isControlAc) {
        this.isControlAc = isControlAc;
    }

    public YesNo getDirectTellerAccess() {
        return directTellerAccess;
    }

    public void setDirectTellerAccess(YesNo directTellerAccess) {
        this.directTellerAccess = directTellerAccess;
    }

    

    public List<XtremeDeductionInterface> getXtremeDeductionInterfaceList() {
        return xtremeDeductionInterfaceList;
    }

    public void setXtremeDeductionInterfaceList(List<XtremeDeductionInterface> xtremeDeductionInterfaceList) {
        this.xtremeDeductionInterfaceList = xtremeDeductionInterfaceList;
    }

    
    
}
