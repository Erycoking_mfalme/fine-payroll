/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "casualcategories")
@NamedQueries({
    @NamedQuery(name = "Casualcategories.findAll", query = "SELECT c FROM Casualcategories c")})
public class Casualcategories implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    
    @Column(name = "CategoryCode",length=10,nullable=false)
    private String categoryCode;
   
    @Column(name = "CategoryName",length=100,nullable=false)
    private String categoryName;
    
    @Column(name = "DefaultRate",nullable=false,scale=2,length=15,columnDefinition="double(15,2) DEFAULT '0.00'")
    private double defaultRate;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryCode")
    private List<Casualstaffmasterfile> casualstaffmasterfileList;

    public Casualcategories() {
    }

    public Casualcategories(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public Casualcategories(String categoryCode, String categoryName, double defaultRate) {
        this.categoryCode = categoryCode;
        this.categoryName = categoryName;
        this.defaultRate = defaultRate;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public double getDefaultRate() {
        return defaultRate;
    }

    public void setDefaultRate(double defaultRate) {
        this.defaultRate = defaultRate;
    }

    public List<Casualstaffmasterfile> getCasualstaffmasterfileList() {
        return casualstaffmasterfileList;
    }

    public void setCasualstaffmasterfileList(List<Casualstaffmasterfile> casualstaffmasterfileList) {
        this.casualstaffmasterfileList = casualstaffmasterfileList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoryCode != null ? categoryCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Casualcategories)) {
            return false;
        }
        Casualcategories other = (Casualcategories) object;
        if ((this.categoryCode == null && other.categoryCode != null) || (this.categoryCode != null && !this.categoryCode.equals(other.categoryCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.Casualcategories[ categoryCode=" + categoryCode + " ]";
    }
    
}
