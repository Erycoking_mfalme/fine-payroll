/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import finextremehr.enums.YesNo;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "GeneralEarnings")
@NamedQueries({
    @NamedQuery(name = "GeneralEarnings.findAll", query = "SELECT g FROM GeneralEarnings g")})
public class GeneralEarnings implements Serializable {   

    private static final long serialVersionUID = 1L;
    @Id   

    @Column(name = "EarningCode",length=10,nullable=false)
    private String earningCode;
   
    @Column(name = "EarningName",length=30)
    private String earningName;    
    
   @Column(name = "TaxRate",columnDefinition=" double(15,2)  DEFAULT '0.00'",nullable=false,precision=15,scale=2)
    private double taxRate; 
    @Column(name = "PeriodCode",length=2)
    private String periodCode;
    
     @Enumerated(EnumType.STRING)
    @Column(name = "Taxable",nullable=false,columnDefinition= "enum('YES','NO')")
    private YesNo taxable = YesNo.YES;
     
     @Enumerated(EnumType.STRING)
    @Column(name = "ConsiderNHIF",columnDefinition=" enum('YES','NO') DEFAULT 'NO'")
    private YesNo considerNHIF;
     
      @Enumerated(EnumType.STRING)
    @Column(name = "IsHousing",columnDefinition="enum('YES','NO') ")
    private YesNo isHousing;
      
       @Enumerated(EnumType.STRING)
    @Column(name = "IsPercOfBasic",nullable=false,columnDefinition=" enum('YES','NO') DEFAULT 'NO'")
    private YesNo isPercOfBasic;
       
       @Enumerated(EnumType.STRING)
    @Column(name = "IsBasicSalary",columnDefinition=" enum('YES','NO') ")
    private YesNo isBasicSalary;

    @Enumerated(EnumType.STRING)
    @Column(name = "DoesNotRecur",nullable=false,columnDefinition=" enum('YES','NO') DEFAULT 'NO'")
    private YesNo doesNotRecur;
    
    @Column(name = "BasicPerc",nullable=false,columnDefinition="  double(15,5)  DEFAULT '0.00000'")
   private double basicPerc;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hSECode")
    private List<HouseAllowanceParams> houseAllowanceParamsList;
    
     @OneToMany(mappedBy = "overTimeCode")
    private List<OverTimeParameters> overTimeParametersList;

    public GeneralEarnings() {
    }

    public GeneralEarnings(String earningCode) {
        this.earningCode = earningCode;
    }

    public GeneralEarnings(String earningCode, String earningName, double taxRate, String periodCode, YesNo taxable, YesNo considerNHIF, YesNo isHousing, YesNo isPercOfBasic, YesNo isBasicSalary, YesNo doesNotRecur, double basicPerc, List<HouseAllowanceParams> houseAllowanceParamsList, List<OverTimeParameters> overTimeParametersList) {
        this.earningCode = earningCode;
        this.earningName = earningName;
        this.taxRate = taxRate;
        this.periodCode = periodCode;
        this.taxable = taxable;
        this.considerNHIF = considerNHIF;
        this.isHousing = isHousing;
        this.isPercOfBasic = isPercOfBasic;
        this.isBasicSalary = isBasicSalary;
        this.doesNotRecur = doesNotRecur;
        this.basicPerc = basicPerc;
        this.houseAllowanceParamsList = houseAllowanceParamsList;
        this.overTimeParametersList = overTimeParametersList;
    }

    public String getEarningCode() {
        return earningCode;
    }

    public void setEarningCode(String earningCode) {
        this.earningCode = earningCode;
    }

    public String getEarningName() {
        return earningName;
    }

    public void setEarningName(String earningName) {
        this.earningName = earningName;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public String getPeriodCode() {
        return periodCode;
    }

    public void setPeriodCode(String periodCode) {
        this.periodCode = periodCode;
    }

    public YesNo getTaxable() {
        return taxable;
    }

    public void setTaxable(YesNo taxable) {
        this.taxable = taxable;
    }

    public YesNo getConsiderNHIF() {
        return considerNHIF;
    }

    public void setConsiderNHIF(YesNo considerNHIF) {
        this.considerNHIF = considerNHIF;
    }

    public YesNo getIsHousing() {
        return isHousing;
    }

    public void setIsHousing(YesNo isHousing) {
        this.isHousing = isHousing;
    }

    public YesNo getIsPercOfBasic() {
        return isPercOfBasic;
    }

    public void setIsPercOfBasic(YesNo isPercOfBasic) {
        this.isPercOfBasic = isPercOfBasic;
    }

    public YesNo getIsBasicSalary() {
        return isBasicSalary;
    }

    public void setIsBasicSalary(YesNo isBasicSalary) {
        this.isBasicSalary = isBasicSalary;
    }

    public YesNo getDoesNotRecur() {
        return doesNotRecur;
    }

    public void setDoesNotRecur(YesNo doesNotRecur) {
        this.doesNotRecur = doesNotRecur;
    }

    public double getBasicPerc() {
        return basicPerc;
    }

    public void setBasicPerc(double basicPerc) {
        this.basicPerc = basicPerc;
    }

    public List<HouseAllowanceParams> getHouseAllowanceParamsList() {
        return houseAllowanceParamsList;
    }

    public void setHouseAllowanceParamsList(List<HouseAllowanceParams> houseAllowanceParamsList) {
        this.houseAllowanceParamsList = houseAllowanceParamsList;
    }

    public List<OverTimeParameters> getOverTimeParametersList() {
        return overTimeParametersList;
    }

    public void setOverTimeParametersList(List<OverTimeParameters> overTimeParametersList) {
        this.overTimeParametersList = overTimeParametersList;
    }

   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (earningCode != null ? earningCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GeneralEarnings)) {
            return false;
        }
        GeneralEarnings other = (GeneralEarnings) object;
        if ((this.earningCode == null && other.earningCode != null) || (this.earningCode != null && !this.earningCode.equals(other.earningCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.company.parameters.GeneralEarnings[ earningCode=" + earningCode + " ]";
    }

   
}
