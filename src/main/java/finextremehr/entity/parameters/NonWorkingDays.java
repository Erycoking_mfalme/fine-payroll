/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author fred
 */
@Entity
@Table(name="NonWorkingDays")
public class NonWorkingDays implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Temporal(javax.persistence.TemporalType.DATE)    
    @Column(name="DefinedDate",nullable=false)
    private Date definedDate;
    
    @Column(name="Description",length=50)
    private String description;

    public NonWorkingDays() {
    }

    public NonWorkingDays(Date definedDate) {
        this.definedDate = definedDate;
    }

    public NonWorkingDays(Date definedDate, String description) {
        this.definedDate = definedDate;
        this.description = description;
    }

    public Date getDefinedDate() {
        return definedDate;
    }

    public void setDefinedDate(Date definedDate) {
        this.definedDate = definedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "NonWorkingDays{" + "definedDate=" + definedDate + ", description=" + description + '}';
    }
    
    
    
}
