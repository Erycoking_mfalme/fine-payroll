/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author fred
 */
@Entity
public class SaccoBranches implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="BranchCode",length=10,nullable=false)
    private String branchCode ;
    
    @Column(name="BranchName",length=20)
    private String branchName;
    
    @Column(name="BranchIP",length=30)
    private String branchIp;
    
    @Column(name="Ctrl",unique=true,columnDefinition=" MEDIUMINT NOT NULL")
    private int ctrl;
    
   
    @OneToMany(mappedBy = "branchCode")
    private List<AccountsList> accountList;

    public SaccoBranches() {
    }

    public SaccoBranches(String branchCode) {
        this.branchCode = branchCode;
    }

    public SaccoBranches(String branchCode, String branchName, String branchIp, int ctrl) {
        this.branchCode = branchCode;
        this.branchName = branchName;
        this.branchIp = branchIp;
        this.ctrl = ctrl;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBranchIp() {
        return branchIp;
    }

    public void setBranchIp(String branchIp) {
        this.branchIp = branchIp;
    }

    public int getCtrl() {
        return ctrl;
    }

    public void setCtrl(int ctrl) {
        this.ctrl = ctrl;
    }

    
        
}
