/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 *
 * @author fred
 */
@Entity
@Table(name = "PAYEBrackets")
@NamedQueries({
    @NamedQuery(name = "PAYEBrackets.findAll", query = "SELECT p FROM PAYEBrackets p")})
public class PAYEBrackets implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    
    @Column(name = "BracketType",length=20,nullable=false)
    private String bracketType;
   
    @Column(name = "StartAmount",nullable=false,columnDefinition=" double(15,2) DEFAULT '0.00'",precision=15,scale=2)
    private double startAmount;
    
    @Column(name = "ToAmount",nullable=false,columnDefinition=" double(15,2) DEFAULT '0.00'",precision=15,scale=2)
    private double toAmount;
    
    @Column(name = "TaxRate",nullable=false,columnDefinition=" double(15,2) DEFAULT '0.00'",precision=15,scale=2)
    private double taxRate;
    
    @Column(name = "PlusAmount",nullable=false,columnDefinition=" double(15,2) DEFAULT '0.00'",precision=15,scale=2)
    private double plusAmount;

    public PAYEBrackets() {
    }

    public PAYEBrackets(String bracketType) {
        this.bracketType = bracketType;
    }

    public PAYEBrackets(String bracketType, double startAmount, double toAmount, double taxRate, double plusAmount) {
        this.bracketType = bracketType;
        this.startAmount = startAmount;
        this.toAmount = toAmount;
        this.taxRate = taxRate;
        this.plusAmount = plusAmount;
    }

    public String getBracketType() {
        return bracketType;
    }

    public void setBracketType(String bracketType) {
        this.bracketType = bracketType;
    }

    public double getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(double startAmount) {
        this.startAmount = startAmount;
    }

    public double getToAmount() {
        return toAmount;
    }

    public void setToAmount(double toAmount) {
        this.toAmount = toAmount;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getPlusAmount() {
        return plusAmount;
    }

    public void setPlusAmount(double plusAmount) {
        this.plusAmount = plusAmount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bracketType != null ? bracketType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PAYEBrackets)) {
            return false;
        }
        PAYEBrackets other = (PAYEBrackets) object;
        if ((this.bracketType == null && other.bracketType != null) || (this.bracketType != null && !this.bracketType.equals(other.bracketType))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.company.parameters.PAYEBrackets[ bracketType=" + bracketType + " ]";
    }
    
}
