/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author fred
 */
@Entity
@Table(name = "casualstaffmasterfile")
@NamedQueries({
    @NamedQuery(name = "Casualstaffmasterfile.findAll", query = "SELECT c FROM Casualstaffmasterfile c")})
public class Casualstaffmasterfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id    
    @Column(name = "IDNo",length=20,nullable=false)
    private String iDNo;    
   
    @Column(name = "Surname",length=30,nullable=false)
    private String surname;
   
    @Column(name = "OtherNames",length=40,nullable=false)
    private String otherNames;
    
    @Column(name = "Address",length=30)
    private String address;
    
    @Column(name = "DateOfLastProcess")
    @Temporal(TemporalType.DATE)
    private Date dateOfLastProcess;
   
    @Column(name = "PhoneNo",length=20,nullable=false)
    private String phoneNo;
   
    @Column(name = "RefereeName",length=20,nullable=false)
    private String refereeName;
    
    @Column(name = "RefereeID",length=20,nullable=false)
    private String refereeID;    
    
    @Column(name = "RefereePhoneNo",length=20,nullable=false)
    private String refereePhoneNo;
    
    @Column(name = "RefereeAddress",length=20,nullable=false)
    private String refereeAddress;
    
    @JoinColumn(name = "CategoryCode", referencedColumnName = "CategoryCode")
    @ManyToOne(optional = false)
    private Casualcategories categoryCode;

    public Casualstaffmasterfile() {
    }

    public Casualstaffmasterfile(String iDNo) {
        this.iDNo = iDNo;
    }

    public Casualstaffmasterfile(String iDNo, String surname, String otherNames, String phoneNo, String refereeName, String refereeID, String refereePhoneNo, String refereeAddress) {
        this.iDNo = iDNo;
        this.surname = surname;
        this.otherNames = otherNames;
        this.phoneNo = phoneNo;
        this.refereeName = refereeName;
        this.refereeID = refereeID;
        this.refereePhoneNo = refereePhoneNo;
        this.refereeAddress = refereeAddress;
    }

    public String getIDNo() {
        return iDNo;
    }

    public void setIDNo(String iDNo) {
        this.iDNo = iDNo;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateOfLastProcess() {
        return dateOfLastProcess;
    }

    public void setDateOfLastProcess(Date dateOfLastProcess) {
        this.dateOfLastProcess = dateOfLastProcess;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getRefereeName() {
        return refereeName;
    }

    public void setRefereeName(String refereeName) {
        this.refereeName = refereeName;
    }

    public String getRefereeID() {
        return refereeID;
    }

    public void setRefereeID(String refereeID) {
        this.refereeID = refereeID;
    }

    public String getRefereePhoneNo() {
        return refereePhoneNo;
    }

    public void setRefereePhoneNo(String refereePhoneNo) {
        this.refereePhoneNo = refereePhoneNo;
    }

    public String getRefereeAddress() {
        return refereeAddress;
    }

    public void setRefereeAddress(String refereeAddress) {
        this.refereeAddress = refereeAddress;
    }

    public Casualcategories getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(Casualcategories categoryCode) {
        this.categoryCode = categoryCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDNo != null ? iDNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Casualstaffmasterfile)) {
            return false;
        }
        Casualstaffmasterfile other = (Casualstaffmasterfile) object;
        if ((this.iDNo == null && other.iDNo != null) || (this.iDNo != null && !this.iDNo.equals(other.iDNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.Casualstaffmasterfile[ iDNo=" + iDNo + " ]";
    }
    
}
