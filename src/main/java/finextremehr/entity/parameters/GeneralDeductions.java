/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import finextremehr.enums.YesNo;
import java.io.Serializable;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;




/**
 *
 * @author fred
 */                                 
@Entity
@Table(name = "GeneralDeductions")
@NamedQueries({
    @NamedQuery(name = "GeneralDeductions.findAll", query = "SELECT g FROM GeneralDeductions g")})
public class GeneralDeductions implements Serializable {    
    
    private static final long serialVersionUID = 1L;
    @Id      
    @Column(name = "DeductionCode",length=10,nullable = false,unique=true)
    private String deductionCode;
   
    @Column(name = "DeductionName",length=30)
    private String deductionName;     
  
    
    @Enumerated(EnumType.STRING)
    @Column(name = "IsLoanDeduction",columnDefinition = " enum('YES','NO') NOT NULL DEFAULT 'NO'")
    private YesNo isLoanDeduction;
   
    @Column(name = "LoanShortName",length=20)
    private String loanShortName;     
   
     @Enumerated(EnumType.STRING)
    @Column(name = "IsDefaultSaccoDeduction",columnDefinition="  enum('YES','NO') NOT NULL DEFAULT 'NO'")
    private YesNo isDefaultSaccoDeduction;
   
    
    @Column(name = "InterestType",length=20)
    private String interestType;    
   
     @Enumerated(EnumType.STRING)
    @Column(name = "ReliefIsPercentage",columnDefinition=" enum('YES','NO') DEFAULT 'YES'")
    private YesNo reliefIsPercentage = YesNo.NO;    
   

    @Column(name = "ReliefName",length = 40)
    private String reliefName;
    
     
    @Column(name="InterestRate",precision=10,scale=2,columnDefinition="double(10,2) NOT NULL DEFAULT '0.00'")
    private double interestRate;
     @Column(name="ReliefRate",precision=10,scale=5,columnDefinition="double(10,5) NOT NULL DEFAULT '0.00000'")
    private double reliefRate;
     @Column(name="MaxReliefAmount",precision=10,scale=5,columnDefinition="double(10,2) NOT NULL DEFAULT '0.00'")
    private double maxReliefAmount;    
    
      @Enumerated(EnumType.STRING)
    @Column(name = "HasBal",columnDefinition=" enum('YES','NO') DEFAULT 'NO'")
    private YesNo hasBal;    
 
   
       @Enumerated(EnumType.STRING)
    @Column(name = "IsIncreasing",columnDefinition=" enum('YES','NO') DEFAULT 'NO' ")
    private YesNo isIncreasing;
    
       
     @Enumerated(EnumType.STRING)
    @Column(name = "IsVoluntaryNSSF",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'NO'")
    private YesNo isVoluntaryNSSF;   
  
    @Enumerated(EnumType.STRING)
    @Column(name = "IsHousing",columnDefinition=" enum('YES','NO') DEFAULT 'NO' ")
    private YesNo isHousing;
    
    @Column(name = "DeductBy",length=20,nullable=false)
    private String deductBy = "Amount";
    
    @Enumerated(EnumType.STRING)
    @Column(name = "DeductAfterTax",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'YES'")
    private YesNo deductAfterTax;
   
      @Enumerated(EnumType.STRING)
    @Column(name = "IsWelfareFund",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'NO' ")
    private YesNo isWelfareFund;
    
   @Enumerated(EnumType.STRING)
    @Column(name = "DoesNotRecur",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'NO' ")
    private YesNo doesNotRecur;    
     
    @Enumerated(EnumType.STRING)
    @Column(name = "IsSaccoLoan",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'NO' ")
    private YesNo isSaccoLoan;
   
    @Enumerated(EnumType.STRING)
    @Column(name = "IsSaccoSaving",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'NO' ")
    private YesNo isSaccoSaving;
    
    @Column(name = "IntType",length=20)
    private String intType;
   
    @OneToOne(mappedBy = "deductionCode")
    private XtremeDeductionInterface xtremeDeductionInterface;

    public GeneralDeductions() {
    }

    public GeneralDeductions(String deductionCode) {
        this.deductionCode = deductionCode;
    }

    public GeneralDeductions(String deductionCode, String deductionName, YesNo isLoanDeduction, String loanShortName, YesNo isDefaultSaccoDeduction, String interestType, YesNo reliefIsPercentage, String reliefName, double interestRate, double reliefRate, double maxReliefAmount, YesNo hasBal, YesNo isIncreasing, YesNo isVoluntaryNSSF, YesNo isHousing, String deductBy, YesNo deductAfterTax, YesNo isWelfareFund, YesNo doesNotRecur, YesNo isSaccoLoan, YesNo isSaccoSaving, String intType, XtremeDeductionInterface xtremeDeductionInterface) {
        this.deductionCode = deductionCode;
        this.deductionName = deductionName;
        this.isLoanDeduction = isLoanDeduction;
        this.loanShortName = loanShortName;
        this.isDefaultSaccoDeduction = isDefaultSaccoDeduction;
        this.interestType = interestType;
        this.reliefIsPercentage = reliefIsPercentage;
        this.reliefName = reliefName;
        this.interestRate = interestRate;
        this.reliefRate = reliefRate;
        this.maxReliefAmount = maxReliefAmount;
        this.hasBal = hasBal;
        this.isIncreasing = isIncreasing;
        this.isVoluntaryNSSF = isVoluntaryNSSF;
        this.isHousing = isHousing;
        this.deductBy = deductBy;
        this.deductAfterTax = deductAfterTax;
        this.isWelfareFund = isWelfareFund;
        this.doesNotRecur = doesNotRecur;
        this.isSaccoLoan = isSaccoLoan;
        this.isSaccoSaving = isSaccoSaving;
        this.intType = intType;
        this.xtremeDeductionInterface = xtremeDeductionInterface;
    }

    public YesNo getIsDefaultSaccoDeduction() {
        return isDefaultSaccoDeduction;
    }

    public void setIsDefaultSaccoDeduction(YesNo isDefaultSaccoDeduction) {
        this.isDefaultSaccoDeduction = isDefaultSaccoDeduction;
    }

    public YesNo getReliefIsPercentage() {
        return reliefIsPercentage;
    }

    public void setReliefIsPercentage(YesNo reliefIsPercentage) {
        this.reliefIsPercentage = reliefIsPercentage;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public double getReliefRate() {
        return reliefRate;
    }

    public void setReliefRate(double reliefRate) {
        this.reliefRate = reliefRate;
    }

    public double getMaxReliefAmount() {
        return maxReliefAmount;
    }

    public void setMaxReliefAmount(double maxReliefAmount) {
        this.maxReliefAmount = maxReliefAmount;
    }

    public YesNo getHasBal() {
        return hasBal;
    }

    public void setHasBal(YesNo hasBal) {
        this.hasBal = hasBal;
    }

    public YesNo getIsIncreasing() {
        return isIncreasing;
    }

    public void setIsIncreasing(YesNo isIncreasing) {
        this.isIncreasing = isIncreasing;
    }

    public YesNo getIsVoluntaryNSSF() {
        return isVoluntaryNSSF;
    }

    public void setIsVoluntaryNSSF(YesNo isVoluntaryNSSF) {
        this.isVoluntaryNSSF = isVoluntaryNSSF;
    }

    public YesNo getIsHousing() {
        return isHousing;
    }

    public void setIsHousing(YesNo isHousing) {
        this.isHousing = isHousing;
    }

    public YesNo getDeductAfterTax() {
        return deductAfterTax;
    }

    public void setDeductAfterTax(YesNo deductAfterTax) {
        this.deductAfterTax = deductAfterTax;
    }

    public YesNo getIsWelfareFund() {
        return isWelfareFund;
    }

    public void setIsWelfareFund(YesNo isWelfareFund) {
        this.isWelfareFund = isWelfareFund;
    }

    public YesNo getDoesNotRecur() {
        return doesNotRecur;
    }

    public void setDoesNotRecur(YesNo doesNotRecur) {
        this.doesNotRecur = doesNotRecur;
    }

    public YesNo getIsSaccoLoan() {
        return isSaccoLoan;
    }

    public void setIsSaccoLoan(YesNo isSaccoLoan) {
        this.isSaccoLoan = isSaccoLoan;
    }

    public YesNo getIsSaccoSaving() {
        return isSaccoSaving;
    }

    public void setIsSaccoSaving(YesNo isSaccoSaving) {
        this.isSaccoSaving = isSaccoSaving;
    }

   

    public String getDeductionCode() {
        return deductionCode;
    }

    public void setDeductionCode(String deductionCode) {
        this.deductionCode = deductionCode;
    }

    public String getDeductionName() {
        return deductionName;
    }

    public void setDeductionName(String deductionName) {
        this.deductionName = deductionName;
    }

    public YesNo getIsLoanDeduction() {
        return isLoanDeduction;
    }

    public void setIsLoanDeduction(YesNo isLoanDeduction) {
        this.isLoanDeduction = isLoanDeduction;
    }

    public String getLoanShortName() {
        return loanShortName;
    }

    public void setLoanShortName(String loanShortName) {
        this.loanShortName = loanShortName;
    }

    


    public String getInterestType() {
        return interestType;
    }

    public void setInterestType(String interestType) {
        this.interestType = interestType;
    }

  

    public String getReliefName() {
        return reliefName;
    }

    public void setReliefName(String reliefName) {
        this.reliefName = reliefName;
    }

   

    public String getDeductBy() {
        return deductBy;
    }

    public void setDeductBy(String deductBy) {
        this.deductBy = deductBy;
    }
    public String getIntType() {
        return intType;
    }
    public void setIntType(String intType) {
        this.intType = intType;
    }
    
     public XtremeDeductionInterface getXtremeDeductionInterface() {
        return xtremeDeductionInterface;
    }


    public void setXtremeDeductionInterface(XtremeDeductionInterface xtremeDeductionInterface) {
        this.xtremeDeductionInterface = xtremeDeductionInterface;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (deductionCode != null ? deductionCode.hashCode() : 0);
        return hash;
    }
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GeneralDeductions)) {
            return false;
        }
        GeneralDeductions other = (GeneralDeductions) object;
        if ((this.deductionCode == null && other.deductionCode != null) || (this.deductionCode != null && !this.deductionCode.equals(other.deductionCode))) {
            return false;
        }
        return true;
    }
    @Override
    public String toString() {
        return "finextremehr.entity.company.GeneralDeductions[ deductionCode=" + deductionCode + " ]";
    }
}

    
