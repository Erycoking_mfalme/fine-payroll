/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "NHIFBrackets")
@NamedQueries({
    @NamedQuery(name = "NHIFBrackets.findAll", query = "SELECT n FROM NHIFBrackets n")})
public class NHIFBrackets implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    
    @Column(name = "id",nullable=false)
    private Integer id;
   
    @Column(name = "BracketType",length=10)
    private String bracketType;
    
    @Column(name = "StartAmount",nullable=false,columnDefinition=" double(15,2) DEFAULT '0.00' ")
    private double startAmount;
    
    @Column(name = "ToAmount",nullable=false,columnDefinition=" double(15,2) DEFAULT '0.00' ")
    private double toAmount;
   
    @Column(name = "TaxRate",nullable=false,columnDefinition=" double(15,2) DEFAULT '0.00' ")
    private double taxRate;
    
    @Column(name = "PlusAmount",nullable=false,columnDefinition=" double(15,2) DEFAULT '0.00' ")
    private double plusAmount;

    public NHIFBrackets() {
    }

    public NHIFBrackets(Integer id) {
        this.id = id;
    }

    public NHIFBrackets(Integer id, double startAmount, double toAmount, double taxRate, double plusAmount) {
        this.id = id;
        this.startAmount = startAmount;
        this.toAmount = toAmount;
        this.taxRate = taxRate;
        this.plusAmount = plusAmount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBracketType() {
        return bracketType;
    }

    public void setBracketType(String bracketType) {
        this.bracketType = bracketType;
    }

    public double getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(double startAmount) {
        this.startAmount = startAmount;
    }

    public double getToAmount() {
        return toAmount;
    }

    public void setToAmount(double toAmount) {
        this.toAmount = toAmount;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public double getPlusAmount() {
        return plusAmount;
    }

    public void setPlusAmount(double plusAmount) {
        this.plusAmount = plusAmount;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NHIFBrackets)) {
            return false;
        }
        NHIFBrackets other = (NHIFBrackets) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.company.parameters.NHIFBrackets[ id=" + id + " ]";
    }
    
}
