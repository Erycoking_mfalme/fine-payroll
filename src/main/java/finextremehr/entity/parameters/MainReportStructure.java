/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "MainReportStructure")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MainReportStructure.findAll", query = "SELECT m FROM MainReportStructure m")
    , @NamedQuery(name = "MainReportStructure.findByRptGroupCode", query = "SELECT m FROM MainReportStructure m WHERE m.rptGroupCode = :rptGroupCode")
    , @NamedQuery(name = "MainReportStructure.findByRptGroupName", query = "SELECT m FROM MainReportStructure m WHERE m.rptGroupName = :rptGroupName")})
public class MainReportStructure implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rptGroupCode")
    private List<ReportHeaders> reportHeadersList;

    private static final long serialVersionUID = 1L;
    @Id    
    @Column(name = "RptGroupCode",length=10,nullable=false)
    private String rptGroupCode;
   
    @Column(name = "RptGroupName",length=50,nullable=false)
    private String rptGroupName;

    public MainReportStructure() {
    }

    public MainReportStructure(String rptGroupCode) {
        this.rptGroupCode = rptGroupCode;
    }

    public MainReportStructure(String rptGroupCode, String rptGroupName) {
        this.rptGroupCode = rptGroupCode;
        this.rptGroupName = rptGroupName;
    }

    public String getRptGroupCode() {
        return rptGroupCode;
    }

    public void setRptGroupCode(String rptGroupCode) {
        this.rptGroupCode = rptGroupCode;
    }

    public String getRptGroupName() {
        return rptGroupName;
    }

    public void setRptGroupName(String rptGroupName) {
        this.rptGroupName = rptGroupName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rptGroupCode != null ? rptGroupCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MainReportStructure)) {
            return false;
        }
        MainReportStructure other = (MainReportStructure) object;
        if ((this.rptGroupCode == null && other.rptGroupCode != null) || (this.rptGroupCode != null && !this.rptGroupCode.equals(other.rptGroupCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.parameters.MainReportStructure[ rptGroupCode=" + rptGroupCode + " ]";
    }

    public List<ReportHeaders> getReportHeadersList() {
        return reportHeadersList;
    }

    public void setReportHeadersList(List<ReportHeaders> reportHeadersList) {
        this.reportHeadersList = reportHeadersList;
    }
    
}
