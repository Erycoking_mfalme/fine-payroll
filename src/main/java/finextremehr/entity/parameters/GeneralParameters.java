/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 *
 * @author fred
 */
@Entity
@Table(name = "GeneralParameters")
@NamedQueries({
    @NamedQuery(name = "GeneralParameters.findAll", query = "SELECT g FROM GeneralParameters g")})
public class GeneralParameters implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    
    @Column(name = "ParamName",length=100,nullable=false)
    private String paramName;
   
    @Column(name = "ParamValue",length=100)
    private String paramValue;

    public GeneralParameters() {
    }

    public GeneralParameters(String paramName) {
        this.paramName = paramName;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paramName != null ? paramName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GeneralParameters)) {
            return false;
        }
        GeneralParameters other = (GeneralParameters) object;
        if ((this.paramName == null && other.paramName != null) || (this.paramName != null && !this.paramName.equals(other.paramName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.company.parameters.GeneralParameters[ paramName=" + paramName + " ]";
    }
    
}
