/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import finextremehr.entity.parameters.AccountsList;
import finextremehr.entity.parameters.GeneralDeductions;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "XtremeDeductionInterface")
@NamedQueries({
    @NamedQuery(name = "XtremeDeductionInterface.findAll", query = "SELECT x FROM XtremeDeductionInterface x")})
public class XtremeDeductionInterface implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id   
    @Column(name = "id",nullable=false)
    private Integer id;
    
  
    @Column(name = "XLoanTypeCode",length=20)
    private String xLoanTypeCode;

    @Column(name = "XSchemeCode",length=20)
    private String xSchemeCode;
    
    @JoinColumn(name = "DeductionCode", referencedColumnName = "DeductionCode")
    @OneToOne
    private GeneralDeductions deductionCode;
    
    @JoinColumn(name = "XtremeSysCode", referencedColumnName = "sysCode")
    @ManyToOne
    private AccountsList xtremeSysCode;

    public XtremeDeductionInterface() {
    }

    public XtremeDeductionInterface(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getXLoanTypeCode() {
        return xLoanTypeCode;
    }

    public void setXLoanTypeCode(String xLoanTypeCode) {
        this.xLoanTypeCode = xLoanTypeCode;
    }

    public String getXSchemeCode() {
        return xSchemeCode;
    }

    public void setXSchemeCode(String xSchemeCode) {
        this.xSchemeCode = xSchemeCode;
    }

    public GeneralDeductions getDeductionCode() {
        return deductionCode;
    }

    public void setDeductionCode(GeneralDeductions deductionCode) {
        this.deductionCode = deductionCode;
    }

    public AccountsList getXtremeSysCode() {
        return xtremeSysCode;
    }

    public void setXtremeSysCode(AccountsList xtremeSysCode) {
        this.xtremeSysCode = xtremeSysCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof XtremeDeductionInterface)) {
            return false;
        }
        XtremeDeductionInterface other = (XtremeDeductionInterface) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.controller.XtremeDeductionInterface[ id=" + id + " ]";
    }
    
}
