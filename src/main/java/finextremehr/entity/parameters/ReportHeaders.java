/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 *
 * @author fred
 */
@Entity
@Table(name = "ReportHeaders")
@NamedQueries({
    @NamedQuery(name = "ReportHeaders.findAll", query = "SELECT r FROM ReportHeaders r")})
public class ReportHeaders implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id    
    @Column(name = "RptTitleCode",length=10,nullable=false)
    private String rptTitleCode;
    
    @Column(name = "RptTitleName",length=50,nullable=false)
    private String rptTitleName;
    
    @JoinColumn(name = "RptGroupCode", referencedColumnName = "RptGroupCode")
    @ManyToOne(optional = false)
    private MainReportStructure rptGroupCode;
    
    @OneToMany(mappedBy = "reportHeader")
    private List<AccountsList> accountList;

    public ReportHeaders() {
    }

    public ReportHeaders(String rptTitleCode) {
        this.rptTitleCode = rptTitleCode;
    }

    public ReportHeaders(String rptTitleCode, String rptTitleName, MainReportStructure rptGroupCode, List<AccountsList> accountList) {
        this.rptTitleCode = rptTitleCode;
        this.rptTitleName = rptTitleName;
        this.rptGroupCode = rptGroupCode;
        this.accountList = accountList;
    }

    public List<AccountsList> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<AccountsList> accountList) {
        this.accountList = accountList;
    }

    
    public String getRptTitleCode() {
        return rptTitleCode;
    }

    public void setRptTitleCode(String rptTitleCode) {
        this.rptTitleCode = rptTitleCode;
    }

    public String getRptTitleName() {
        return rptTitleName;
    }

    public void setRptTitleName(String rptTitleName) {
        this.rptTitleName = rptTitleName;
    }

    public MainReportStructure getRptGroupCode() {
        return rptGroupCode;
    }

    public void setRptGroupCode(MainReportStructure rptGroupCode) {
        this.rptGroupCode = rptGroupCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rptTitleCode != null ? rptTitleCode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReportHeaders)) {
            return false;
        }
        ReportHeaders other = (ReportHeaders) object;
        if ((this.rptTitleCode == null && other.rptTitleCode != null) || (this.rptTitleCode != null && !this.rptTitleCode.equals(other.rptTitleCode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.parameters.ReportHeaders[ rptTitleCode=" + rptTitleCode + " ]";
    }
    
}
