/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author fred
 */
@Entity
@Table(name="TypesOfLeave")
public class TypesOfLeave implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name="LeaveCode",length=20,nullable=false,unique=true)
    private String leaveCode;
    
    @Column(name="LeaveName",length=50)
    private String leaveName;

    public TypesOfLeave(String leaveCode) {
        this.leaveCode = leaveCode;
    }

    public TypesOfLeave() {
    }

    public TypesOfLeave(String leaveCode, String leaveName) {
        this.leaveCode = leaveCode;
        this.leaveName = leaveName;
    }

    public String getLeaveCode() {
        return leaveCode;
    }

    public void setLeaveCode(String leaveCode) {
        this.leaveCode = leaveCode;
    }

    public String getLeaveName() {
        return leaveName;
    }

    public void setLeaveName(String leaveName) {
        this.leaveName = leaveName;
    }

    
    

    @Override
    public String toString() {
        return "finextremehr.entity.parameters.TypesOfLeave[ id=" + leaveCode + " ]";
    }
    
}
