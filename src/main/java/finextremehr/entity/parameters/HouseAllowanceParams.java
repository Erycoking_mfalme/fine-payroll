/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import finextremehr.enums.CalculationBase;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 *
 * @author fred
 */
@Entity
@Table(name = "HouseAllowanceParams")
@NamedQueries({
    @NamedQuery(name = "HouseAllowanceParams.findAll", query = "SELECT h FROM HouseAllowanceParams h")})
public class HouseAllowanceParams implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
   
    @Column(name = "id",nullable=false)
    private Integer id;
   
     @Enumerated(EnumType.STRING)
    @Column(name = "CalculateUsing",columnDefinition="enum('PercBasic','PercGross','Amount')")
    private CalculationBase calculateUsing;
   
    @Column(name = "Rate",nullable=false,columnDefinition="double(15,2) DEFAULT '0.00'")
    private double rate;
    
    @JoinColumn(name = "HSECode", referencedColumnName = "EarningCode")
    @ManyToOne(optional = false)
    private GeneralEarnings hSECode;

    public HouseAllowanceParams() {
    }

    public HouseAllowanceParams(Integer id) {
        this.id = id;
    }

    public HouseAllowanceParams(Integer id, CalculationBase calculateUsing, double rate, GeneralEarnings hSECode) {
        this.id = id;
        this.calculateUsing = calculateUsing;
        this.rate = rate;
        this.hSECode = hSECode;
    }

    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CalculationBase getCalculateUsing() {
        return calculateUsing;
    }

    public void setCalculateUsing(CalculationBase calculateUsing) {
        this.calculateUsing = calculateUsing;
    }

    public GeneralEarnings gethSECode() {
        return hSECode;
    }

    public void sethSECode(GeneralEarnings hSECode) {
        this.hSECode = hSECode;
    }

    
    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public GeneralEarnings getHSECode() {
        return hSECode;
    }

    public void setHSECode(GeneralEarnings hSECode) {
        this.hSECode = hSECode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HouseAllowanceParams)) {
            return false;
        }
        HouseAllowanceParams other = (HouseAllowanceParams) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "finextremehr.entity.company.parameters.HouseAllowanceParams[ id=" + id + " ]";
    }
    
}
