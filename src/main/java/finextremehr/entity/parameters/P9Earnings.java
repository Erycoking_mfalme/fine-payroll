/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;

/**
 *
 * @author fred
 */
@Entity
public class P9Earnings implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
   @Column(name="EarningCode",length=10,nullable=false)
    private String earningCode;
    
    @Column(name="EarningName",length=50)
    private String earningName;

    public P9Earnings() {
    }

    
    public P9Earnings(String earningCode) {
        this.earningCode = earningCode;
    }

    public P9Earnings(String earningCode, String earningName) {
        this.earningCode = earningCode;
        this.earningName = earningName;
    }

    public String getEarningCode() {
        return earningCode;
    }

    public void setEarningCode(String earningCode) {
        this.earningCode = earningCode;
    }

    public String getEarningName() {
        return earningName;
    }

    public void setEarningName(String earningName) {
        this.earningName = earningName;
    }

   
    

  
    
}
