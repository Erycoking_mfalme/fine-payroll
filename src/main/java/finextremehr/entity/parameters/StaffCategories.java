/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.entity.parameters;

import finextremehr.enums.YesNo;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author fred
 */
@Entity
@Table(name = "StaffCategories")
public class StaffCategories implements Serializable {

    private static final long serialVersionUID = 1L;
   
   
     @Id
    @Column(name="CategoryCode",length=20,unique=true,nullable=false)
    private String categoryCode;
    
    @Column(name="CategoryName",length=20,nullable=false)
    private String categoryName;
    
  
    @Column(name="HasRelief",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'NO'")
    private YesNo hasRelief;
    
   
     @Column(name="AutoHousing",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'NO'")
    private YesNo autoHousing;
     
   
    @Column(name="ExcemptedFromPaye",columnDefinition=" enum('YES','NO') NOT NULL DEFAULT 'NO'")
    private YesNo excemptedFromPaye;

    public StaffCategories() {
    }

    public StaffCategories(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    

    public StaffCategories( String categoryCode, String categoryName, YesNo hasRelief, YesNo autoHousing, YesNo excemptedFromPaye) {
        
        this.categoryCode = categoryCode;
        this.categoryName = categoryName;
        this.hasRelief = hasRelief;
        this.autoHousing = autoHousing;
        this.excemptedFromPaye = excemptedFromPaye;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public YesNo getHasRelief() {
        return hasRelief;
    }

    public void setHasRelief(YesNo hasRelief) {
        this.hasRelief = hasRelief;
    }

    public YesNo getAutoHousing() {
        return autoHousing;
    }

    public void setAutoHousing(YesNo autoHousing) {
        this.autoHousing = autoHousing;
    }

    public YesNo getExcemptedFromPaye() {
        return excemptedFromPaye;
    }

    public void setExcemptedFromPaye(YesNo excemptedFromPaye) {
        this.excemptedFromPaye = excemptedFromPaye;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.categoryCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final StaffCategories other = (StaffCategories) obj;
        if (!Objects.equals(this.categoryCode, other.categoryCode)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "StaffCategories{" + "categoryCode=" + categoryCode + '}';
    }

   

    
   
    
}
