/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.service;

import finextremehr.entity.company.Branch;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author fred
 */
@Stateless
public class BranchesFacade extends AbstractFacade<Branch> {

    @PersistenceContext(unitName = "ke.co.lanstar_FineXtremeHR_war_1.00PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BranchesFacade() {
        super(Branch.class);
    }
    
}
