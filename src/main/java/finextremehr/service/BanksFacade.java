/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.service;

import finextremehr.entity.company.Bank;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author fred
 */
@Stateless
public class BanksFacade extends AbstractFacade<Bank> {

    @PersistenceContext(unitName = "ke.co.lanstar_FineXtremeHR_war_1.00PU")
    private EntityManager em;
    
    

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
   
    public BanksFacade() {
        super(Bank.class);
    }
    
}
