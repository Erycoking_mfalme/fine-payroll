/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.service;

import finextremehr.entity.parameters.PAYEBrackets;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author kadan
 */
@Stateless
public class PAYEBracketsFacade extends AbstractFacade<PAYEBrackets> {

    @PersistenceContext(unitName = "ke.co.lanstar_FineXtremeHR_war_1.00PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PAYEBracketsFacade() {
        super(PAYEBrackets.class);
    }
    
}
