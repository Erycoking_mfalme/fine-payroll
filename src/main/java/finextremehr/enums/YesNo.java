/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.enums;

/**
 *
 * @author erycoking
 */
public enum YesNo {
    YES("YES"),
    NO("NO");
    
   private String name;
   
   private YesNo(String name){
       this.name = name;
   }
   
   public String getName(){
       return name;
   }
    
   public static YesNo nameUsed(String shortName) {
        switch (shortName) {
        case "YES":
            return YesNo.YES;
 
        case "NO":
            return YesNo.NO;
 
        
 
        default:
            throw new IllegalArgumentException("ShortName [" + shortName
                    + "] not supported.");
        }
}
}
