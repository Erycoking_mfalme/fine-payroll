/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/**
 *
 * @author fred
 */
//@Converter(autoApply=true)
public class YesNoConverter  implements AttributeConverter<YesNo, String> {

    @Override
    public String convertToDatabaseColumn(YesNo x) {
        
        return x.getName();
    }

    @Override
    public YesNo convertToEntityAttribute(String y) {
        return YesNo.nameUsed(y);
    }
    
}
