/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.personalManagement;

import finextremehr.entity.personalManagement.RentPaid;
import finextremehr.service.RentPaidFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author erycoking
 */
@SessionScoped
@Named(value = "rentpaidController")
public class RentPaidController implements Serializable{
    
    
    @EJB
    private RentPaidFacade rf;
    
    private RentPaid rentPaid;

    public RentPaidController() {
    }

    public RentPaidController(RentPaid rentPaid) {
        this.rentPaid = rentPaid;
    }

    public RentPaid getRentPaid() {
        return rentPaid;
    }

    public void setRentPaid(RentPaid rentPaid) {
        this.rentPaid = rentPaid;
    }

    public RentPaidFacade getRf() {
        return rf;
    }

    public void setRf(RentPaidFacade rf) {
        this.rf = rf;
    }
    
    public String returnViewAll(){
        return "viewAllRentpaid";
    }
    
    public List<RentPaid> getAllRentpaid(){
        return this.rf.findAll();
    }
    
    public String returnCreateView(){
        return "addRentPaid";
    }
    
    public String addRentPaid(){
        this.rf.create(this.rentPaid);
        this.rentPaid = new RentPaid();
        return "viewRentPaid";
    }
    
    public String getEditRentpaid(RentPaid rentPaid){
        this.rentPaid  = rentPaid;
        return "editRentPaid";
    }
    
    public String updateRentPaid(){
        this.rf.edit(this.rentPaid);
        return "viewRentPaid";
    }
    
    public RentPaid getCurrentRentPaid(){
        return this.rf.find(this.rentPaid);
    }
    
    
    
    
}
