/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.personalManagement;

import finextremehr.entity.parameters.GeneralDeductions;
import finextremehr.service.GeneralDeductionsFacade;
import javax.inject.Named;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author fred
 */
//@Named(value = "staffDeductionController")
@ManagedBean(name="staffDeductionController")
@ViewScoped
public class StaffDeductionController implements Serializable {

    @EJB
    private GeneralDeductionsFacade generalDeductionsFacade;
    
    private String deductionCode;

    public String getDeductionCode() {
        return deductionCode;
    }

    public void setDeductionCode(String deductionCode) {
        this.deductionCode = deductionCode;
    }
    
    
    
    //To hold deduction code and deduction name
   private  Map<String,String> deductions = new HashMap<>();

    public Map<String, String> getDeductions() {
        return deductions;
    }

   
    
    

    /**
     * Creates a new instance of StaffDeduction
     */
    
   
    public StaffDeductionController() {
    }
    
     
    //Populate allDeductions map
    
     @PostConstruct
    public void init(){
        List<GeneralDeductions> allDeductions = generalDeductionsFacade.findAll();        
        for(GeneralDeductions deduc : allDeductions){
            deductions.put(deduc.getDeductionCode()+":"+deduc.getDeductionName(), deduc.getDeductionCode()+" : "+deduc.getDeductionName());
        }
               
        
        
    }
   
    
}
