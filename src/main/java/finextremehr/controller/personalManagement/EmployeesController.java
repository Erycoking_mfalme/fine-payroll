/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.personalManagement;

import finextremehr.entity.company.Bank;
import finextremehr.entity.company.Branch;
import finextremehr.entity.company.Department;
import finextremehr.entity.company.Jobgroups;
import finextremehr.entity.company.Sections;
import finextremehr.entity.parameters.StaffCategories;
import finextremehr.entity.personalManagement.Employees;

import finextremehr.service.EmployeesFacade;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;



/**
 *
 * @author erycoking
 */

@Named(value = "empController")
@SessionScoped
public class EmployeesController implements Serializable{
    
    @EJB
    private EmployeesFacade ef;
    
    private Employees employees = new Employees();

    public EmployeesController() {
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public EmployeesFacade getEf() {
        return ef;
    }

    public void setEf(EmployeesFacade ef) {
        this.ef = ef;
    }
    
    public String viewEmployees(Employees employees){
        this.employees = employees;
        return "viewEmployees";
    }
    
    public List<Employees> getAllEmployees(){
        return this.ef.findAll();
    }
    
    public Employees getAnEmployees(){
        return this.ef.find(this.employees);
    }
    
    public String getCreatePage(){
        this.employees = new Employees();
        return "addEmployee";
    }
    
    public String addEmployees(){
        
        this.employees.setEmploymentDate(new Date());
        this.employees.setDOB(new Date());
        
        StaffCategories sc = new StaffCategories();
        sc.setCategoryCode("001");
        sc.setCategoryName("member");    
        
        this.employees.setStaffType(sc);
        Bank bank = new Bank();
        bank.setBankCode("001");
        bank.setBankName("001");
        
        Branch br = new Branch();
        br.setBank(bank);
        br.setBranchCode("001");
        br.setBranchName("nairobi");
        
        List<Branch> branchs = new ArrayList<>(
                Arrays.asList(br)
        );
        bank.setBranches(branchs);
        this.employees.setBankCode(bank);

        Jobgroups jb = new Jobgroups();
        jb.setJobGroupCode("001");
        jb.setJobGroupName("001");
        this.employees.setJobGroupCode(jb);
        Department dep = new Department();
        dep.setDepartmentCode("001");
        dep.setDepartmentName("kariako");
        this.employees.setDepartmentCode(dep);
        
        Sections sec = new Sections();
        sec.setDepartment(dep);
        sec.setSectionCode("001");
        sec.setSectionName("king");
        List<Sections> secList = new ArrayList<>(
                Arrays.asList(sec)
        );
        dep.setSections(secList);
        this.employees.setBankCode(bank);

        this.ef.create(this.employees);
        this.employees = new Employees();
        return "viewEmployees";
    }
    
    public String editEmployee(Employees employees){
        this.employees = employees;
        return "editEmployee";
    }
    
    public String updateEmployee(){
        this.ef.edit(this.employees);
        this.employees = new Employees();
        return "viewEmployees";
    }
    
    public String deletePage(Employees employees){
        this.employees = employees;
        return "deletePage";
    }
    
    public String deleteEmployee(){
        this.ef.remove(this.employees);
        this.employees = new Employees();
        return "";
    }
}
