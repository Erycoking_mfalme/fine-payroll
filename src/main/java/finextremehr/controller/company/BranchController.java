/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.company;


import finextremehr.entity.company.Branch;
import finextremehr.service.BranchesFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;



/**
 *
 * @author kadan
 */
@Named(value = "branchController")
@SessionScoped
public class BranchController implements Serializable {

    @EJB
    private BranchesFacade branchesFacade;
    
    private Branch branch = new Branch();
    
     public BranchController() {
    }
    
    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

   //Display All 
    public List<Branch> findAll(){
        return this.branchesFacade.findAll();
    }
    
    //Add new 
    public String add(){
        this.branchesFacade.create(this.branch);
        this.branch = new Branch();
        return "viewBranches";
    }
    //Loading  to the Edit page
    public String edit(Branch branch){
        this.branch = branch;
        return "editBranch";
    }
    //Updating  Details
    public String update(){
        this.branchesFacade.edit(this.branch);
        this.branch = new Branch();
        return "viewBranches";
    }
    
    //Delete Bank
    public void delete(Branch branch){
        this.branchesFacade.remove(branch);
        
    }  
}
