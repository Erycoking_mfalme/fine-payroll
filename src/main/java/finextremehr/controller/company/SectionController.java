/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.company;


import finextremehr.entity.company.Department;
import finextremehr.entity.company.Sections;
import finextremehr.service.DepartmentFacade;
import finextremehr.service.SectionsFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author fred
 */
@Named(value = "sectionController")
@SessionScoped
public class SectionController implements Serializable {

    @EJB
    private DepartmentFacade departmentFacade;
    private String DeptCode;

    @EJB
    private SectionsFacade sectionsFacade;
    
    private Sections section = new Sections();
    
    
    
    
    public SectionController() {
    }

    public String getDeptCode() {
        return DeptCode;
    }

    public void setDeptCode(String DeptCode) {
        this.DeptCode = DeptCode;
    }

    

    public Sections getSection() {
        return section;
    }

    public void setSection(Sections section) {
        this.section = section;
    }
     //Add new Section
    public String add(){
        Department dept = departmentFacade.find(this.DeptCode);
        this.section.setDepartment(dept);
        this.sectionsFacade.edit(this.section);
        this.section = new Sections();
        return "viewSections";
    }
    //Loading Department to the Edit page
    public String edit(Sections section){
        this.section = section;
        return "editSection";
    }
    //Updating Department Details
    public String update(){
        this.sectionsFacade.edit(this.section);
        this.section= new Sections();
        return "viewSections";
    }
    
    //Delete Section
    public void delete(Sections section){
        this.sectionsFacade.remove(section);
        
    }
    
     //Display All sections
    public List<Sections> findAll(){
        return this.sectionsFacade.findAll();
    }
    
    //Get All Department

   
    public  List<Department> getAllDepartments(){
        return departmentFacade.findAll();
    }
    
    
}
