/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.company;

import finextremehr.entity.company.Bank;
import finextremehr.service.BanksFacade;
import java.io.IOException;
import java.io.InputStream;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;


import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.event.FileUploadEvent;


/**
 *
 * @author fred
 */
@Named(value="fileUploadManagedBean")
@SessionScoped
public class FileUploadManagedBean implements Serializable {

    @EJB
    private BanksFacade banksFacade;
    
    
    
    

//    UploadedFile file;
//
//    public UploadedFile getFile() {
//        return file;
//    }
//
//    public void setFile(UploadedFile file) {
//        this.file = file;
//    }
//    
//    public String dummyAction(){
//		System.out.println("Uploaded File Name Is :: "+file.getFileName()+" :: Uploaded File Size :: "+file.getSize());
//		return "";
//	}
//    public FileUploadManagedBean() {
//    }
    public void handleFileUpload(FileUploadEvent event) throws InvalidFormatException {
        
        InputStream file;
        
        //To hold excel cell values
          List items = new ArrayList();
       
        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = null;
        try {
            file = event.getFile().getInputstream();
            workbook = WorkbookFactory.create(file);
        } catch (IOException e) {
           
        }
        
        //Get the sheet at Index zero
       Sheet sheet = workbook.getSheetAt(0);    
      
       //obtain a rowIterator
       Iterator<Row> rowIterator = sheet.iterator();
       
       
       // Calendar calendar = new GregorianCalendar();
        while (rowIterator.hasNext()) {
            
        Row row = rowIterator.next();

        Iterator<Cell> cellIterator = row.cellIterator();
       
        while (cellIterator.hasNext()) {
           Cell cell = cellIterator.next();
               
           if(cell.getCellTypeEnum() == CellType.STRING){
              items.add(cell.getStringCellValue());
           }else
               if(cell.getCellTypeEnum() == CellType.NUMERIC ){
                   items.add(cell.getNumericCellValue());
            }else
                if(cell.getCellTypeEnum() == CellType.ERROR){
                    System.err.println("Error");
                }
        
           
            
             }
        
          
         
        }
        
          insertRowDb(items);
    
    }
    
    public void insertRowDb(List cellValues){
        Bank bank = new Bank();
        
        int i = 0;
        while(i <  cellValues.size()){
            bank.setBankCode(cellValues.get(i).toString());
            bank.setBankName(cellValues.get(i+1).toString());
            
            banksFacade.create(bank);
            
            i+=2;
        }
    }
}
