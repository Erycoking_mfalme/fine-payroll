/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.company;

import finextremehr.entity.company.Bank;
import finextremehr.entity.company.Branch;
import finextremehr.service.BanksFacade;
import finextremehr.service.BranchesFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author kadan
 */
@Named(value = "bankController")
@SessionScoped
public class BankController implements Serializable {

    @EJB
    private BranchesFacade branchesFacade;

    @EJB
    private BanksFacade banksFacade;
    private Branch branch = new Branch();
    private Bank bank        = new Bank();
    
        
    public BankController() {
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        //System.out.println(bank);
        this.bank = bank;
    }
    
    public Branch getBranch() {
        return this.branch;
    }
    
    public void setBranch(Branch branch){
        this.branch = branch;
    }
    
    //Display All Bank
    public List<Bank> findAll(){
        return this.banksFacade.findAll();
    }
    
    //Add new Bank
    public void add(){
        this.banksFacade.create(this.bank);
        this.bank = new Bank();
    }
    
    public void addBranch(String bankCode) {
        Bank newBank = this.banksFacade.find(bankCode);
        this.branch.setBank(newBank);
        
        newBank.getBranches().add(this.branch);
        this.banksFacade.edit(newBank);
        this.branch = new Branch();
        this.bank     = new Bank();
    }
    
    public List<Branch> getBranchesByBank(Bank bank) {
        return bank.getBranches();
    }
    
    
    //Loading Bank to the Edit page
    public void  editBank(Bank bank){
        this.bank = bank;
    }
    
    public void  editBranch(Branch branch){
        this.branch = branch;
    }
    
    
    //Updating Bank Details
    public void updateBank(){
        this.banksFacade.edit(this.bank);
        this.bank = new Bank();
    }
    
    public void updateBranch() {
        this.branchesFacade.edit(this.branch);
        this.branch = new Branch();
    }
    
    
    //Delete Bank
    public void deleteBank(Bank bank){
        this.banksFacade.remove(bank);
    }
    
    public void deleteBranch(Branch branch){
        Bank currentBank = branch.getBank();
        currentBank.getBranches().remove(branch);
        this.banksFacade.edit(currentBank);
    }
}
