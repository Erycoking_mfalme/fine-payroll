/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.company;

import finextremehr.entity.company.Awards;
import finextremehr.service.AwardsFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;

import java.util.List;

/**
 *
 * @author kadan
 */
@Named(value = "awardsController")
@SessionScoped
public class AwardsController implements Serializable {

    @EJB
    private AwardsFacade awardsFacade;
    private Awards award = new Awards();
    
    public AwardsController() {
    }

    public Awards getAward() {
        return award;
    }

    public void setAward(Awards award) {
        this.award = award;
    }
    
    //Display All Banks
    public List<Awards> findAll(){
        return this.awardsFacade.findAll();
    }
    
    //Add new award
    public String add(){
        this.awardsFacade.create(this.award);
        this.award = new Awards();
        return "viewAwards";
    }
    //Loading award to the Edit page
    public String edit(Awards award){
        this.award = award;
        return "editAward";
    }
    //Updating award Details
    public String update(){
        this.awardsFacade.edit(this.award);
        this.award = new Awards();
        return "viewAwards";
    }
    
    //Delete award
    public void delete(Awards award){
        this.awardsFacade.remove(award);        
    } 
    
}
