/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.company;

import finextremehr.entity.company.Department;
import finextremehr.entity.company.Sections;
import finextremehr.service.DepartmentFacade;
import finextremehr.service.SectionsFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author fred
 */
@Named(value = "departmentController")
@SessionScoped
public class DepartmentController implements Serializable {

    @EJB
    private DepartmentFacade departmentFacade;
    @EJB
    private SectionsFacade sectionFacade;
    private Department department = new Department();
    private Sections section                 = new Sections();

    
    public DepartmentController() {
    }
    
    public DepartmentFacade getDepartmentFacade() {
        return departmentFacade;
    }
    
    public void editDepartment(Department department) {
        this.department = department;
    }
    
    public void editSection(Sections section) {
        this.section = section;
    }

    public void setDepartmentFacade(DepartmentFacade departmentFacade) {
        this.departmentFacade = departmentFacade;
    }

    public Sections getSection() {
        return section;
    }

    public void setSection(Sections section) {
        this.section = section;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
    
     //Add new Department
    public void  addDepartment(){
        this.departmentFacade.create(this.department);
        this.department = new Department();
    }
    
    
    //Updating Department Details
    public void updateDepartment() {
        this.departmentFacade.edit(this.department);
        this.department = new Department();
    }
    
    public void updateSection() {
        this.sectionFacade.edit(this.section);
        this.section = new Sections();
    }
    
    //Delete Department
    public void deleteDepartment(Department department) {
        this.departmentFacade.remove(department);
    }
    
     //Display All departments
    public List<Department> findAll(){
        return this.departmentFacade.findAll();
    }
    
    public List<Sections> getSectionsByDepartment(Department dep) {
        return dep.getSections();
    }
}
