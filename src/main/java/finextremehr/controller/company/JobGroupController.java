/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.company;

import finextremehr.entity.company.Jobgroups;
import finextremehr.service.JobGroupsFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author fred
 */
@Named(value = "jobGroupController")
@SessionScoped
public class JobGroupController implements Serializable {

    @EJB
    private JobGroupsFacade jobGroupsFacade;
    
    private Jobgroups jobGroup = new Jobgroups();

    public Jobgroups getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(Jobgroups jobGroup) {
        this.jobGroup = jobGroup;
    }

    
    public JobGroupController() {
    }
    
    
    //Add new jobgroup
    public String add(){
        this.jobGroupsFacade.create(this.jobGroup);
        this.jobGroup = new Jobgroups();
        return "viewJobgroups";
    }
    //Loading jobgroup to the Edit page
    public String edit(Jobgroups jobgroups){
        this.jobGroup = jobgroups;
        return "editJobgroup";
    }
    //Updating jobgroup Details
    public String update(){
        this.jobGroupsFacade.edit(this.jobGroup);
        this.jobGroup= new Jobgroups();
        return "viewJobgroups";
    }
    
    //Delete jobgroup
    public void delete(Jobgroups jobgroup){
        this.jobGroupsFacade.remove(jobgroup);
        
    }
    
     //Display All jobgroups
    public List<Jobgroups> findAll(){
        return this.jobGroupsFacade.findAll();
    }
}
