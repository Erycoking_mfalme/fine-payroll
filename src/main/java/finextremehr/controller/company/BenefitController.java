/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.company;

import finextremehr.entity.company.Benefits;
import finextremehr.service.BenefitsFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;

import java.util.List;

/**
 *
 * @author kadan
 */
@Named(value = "benefitController")
@SessionScoped
public class BenefitController implements Serializable {

    @EJB
    private BenefitsFacade benefitsFacade;
    private Benefits benefit = new Benefits();

    public BenefitController() {
    }

    public Benefits getBenefit() {
        return benefit;
    }

    public void setBenefit(Benefits benefit) {
        this.benefit = benefit;
    }
    
    //Display All 
    public List<Benefits> findAll(){
        return this.benefitsFacade.findAll();
    }
    
    //Add new 
    public String add(){
        this.benefitsFacade.create(this.benefit);
        this.benefit = new Benefits();
        return "viewBenefits";
    }
    //Loading  to the Edit page
    public String edit(Benefits branch){
        this.benefit = branch;
        return "editBenefit";
    }
    //Updating  Details
    public String update(){
        this.benefitsFacade.edit(this.benefit);
        this.benefit = new Benefits();
        return "viewBenefits";
    }
    
    //Delete Bank
    public void delete(Benefits benefit){
        this.benefitsFacade.remove(benefit);
        
    }  
}
