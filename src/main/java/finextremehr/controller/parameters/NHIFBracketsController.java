package finextremehr.controller.parameters;

import finextremehr.entity.parameters.NHIFBrackets;
import finextremehr.service.NHIFBracketsFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

@Named(value = "nHIFBracketsController")
@SessionScoped
public class NHIFBracketsController implements Serializable {

    @EJB
    private NHIFBracketsFacade nHIFBracketsFacade;
    private NHIFBrackets nHIFBracket = new NHIFBrackets();

    public NHIFBracketsController() {
    }

    public NHIFBrackets getnHIFBracket() {
        return nHIFBracket;
    }

    public void setnHIFBracket(NHIFBrackets nHIFBracket) {
        this.nHIFBracket = nHIFBracket;
    }
    
     //Display All 
    public List<NHIFBrackets> findAll(){
        return this.nHIFBracketsFacade.findAll();
    }
    
    //Add new award
    public String add(){
        this.nHIFBracketsFacade.create(this.nHIFBracket);
        this.nHIFBracket = new NHIFBrackets();
        return "viewNHIFBrackets";
    }
    //Loading the Edit page
    public String edit(NHIFBrackets nHIFBracket){
        this.nHIFBracket = nHIFBracket;
        return "editNHIFBracket";
    }
    //Updating  Details
    public String update(){
        this.nHIFBracketsFacade.edit(this.nHIFBracket);
        this.nHIFBracket = new NHIFBrackets();
        return "viewNHIFBrackets";
    }
    
    //Delete 
    public void delete(NHIFBrackets nHIFBracket){
        this.nHIFBracketsFacade.remove(nHIFBracket);
    } 
}
