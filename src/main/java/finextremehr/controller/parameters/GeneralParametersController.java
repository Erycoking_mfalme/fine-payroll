package finextremehr.controller.parameters;

import finextremehr.entity.parameters.GeneralParameters;
import finextremehr.service.GeneralParametersFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author kadan
 */
@Named(value = "generalParametersController")
@SessionScoped
public class GeneralParametersController implements Serializable {

    @EJB
    private GeneralParametersFacade generalParametersFacade;
    private GeneralParameters generalParameter = new GeneralParameters();

    private double personalRelief;
    private double NSSFFigure;
    private double maximumNSSF;
    private boolean deductPensionAfterTax;
    private boolean deductNSSFAfterTax;
    private boolean deductNHIFAfterTax;
    private boolean NSSFIsPercentage;
    private boolean calculateNHIFFromBasicSalary;
    private double maxPensionDeductableB4Tax;
    private boolean deductProvidentAfterTax;
    private boolean casualRateIsPerDay;
    private boolean housingPercentageOfGross;
    private double housingPercentage;
    private String payrollPrefix;
    
    public GeneralParametersController() {         
    }

    public GeneralParameters getGeneralParameter() {
        return generalParameter;
    }

    public void setGeneralParameter(GeneralParameters generalParameter) {
        this.generalParameter = generalParameter;
    }

    public double getMaximumNSSF() {
        return maximumNSSF;
    }

    public void setMaximumNSSF(double maximumNSSF) {
        this.maximumNSSF = maximumNSSF;
    }

    public boolean isDeductNSSFAfterTax() {
        return deductNSSFAfterTax;
    }

    public void setDeductNSSFAfterTax(boolean deductNSSFAfterTax) {
        this.deductNSSFAfterTax = deductNSSFAfterTax;
    }

    public boolean isDeductNHIFAfterTax() {
        return deductNHIFAfterTax;
    }

    public void setDeductNHIFAfterTax(boolean deductNHIFAfterTax) {
        this.deductNHIFAfterTax = deductNHIFAfterTax;
    }

    public boolean isNSSFIsPercentage() {
        return NSSFIsPercentage;
    }

    public void setNSSFIsPercentage(boolean NSSFIsPercentage) {
        this.NSSFIsPercentage = NSSFIsPercentage;
    }

    public boolean isCalculateNHIFFromBasicSalary() {
        return calculateNHIFFromBasicSalary;
    }

    public void setCalculateNHIFFromBasicSalary(boolean calculateNHIFFromBasicSalary) {
        this.calculateNHIFFromBasicSalary = calculateNHIFFromBasicSalary;
    }

    public double getMaxPensionDeductableB4Tax() {
        return maxPensionDeductableB4Tax;
    }

    public void setMaxPensionDeductableB4Tax(double maxPensionDeductableB4Tax) {
        this.maxPensionDeductableB4Tax = maxPensionDeductableB4Tax;
    }

    public boolean isDeductProvidentAfterTax() {
        return deductProvidentAfterTax;
    }

    public void setDeductProvidentAfterTax(boolean deductProvidentAfterTax) {
        this.deductProvidentAfterTax = deductProvidentAfterTax;
    }

    public boolean isCasualRateIsPerDay() {
        return casualRateIsPerDay;
    }

    public void setCasualRateIsPerDay(boolean casualRateIsPerDay) {
        this.casualRateIsPerDay = casualRateIsPerDay;
    }

    public boolean isHousingPercentageOfGross() {
        return housingPercentageOfGross;
    }

    public void setHousingPercentageOfGross(boolean housingPercentageOfGross) {
        this.housingPercentageOfGross = housingPercentageOfGross;
    }

    public double getHousingPercentage() {
        return housingPercentage;
    }

    public void setHousingPercentage(double housingPercentage) {
        this.housingPercentage = housingPercentage;
    }
    

    public GeneralParametersFacade getGeneralParametersFacade() {
        return generalParametersFacade;
    }

    public void setGeneralParametersFacade(GeneralParametersFacade generalParametersFacade) {
        this.generalParametersFacade = generalParametersFacade;
    }

    public double getPersonalRelief() {
        
        return personalRelief;
    }

    public void setPersonalRelief(double personalRelief) {
        this.personalRelief = personalRelief;
    }

    public double getNSSFFigure() {
        return NSSFFigure;
    }

    public void setNSSFFigure(double NSSFFigure) {
        this.NSSFFigure = NSSFFigure;
    }   

    public boolean isDeductPensionAfterTax() {
        return deductPensionAfterTax;
    }

    public void setDeductPensionAfterTax(boolean deductPensionAfterTax) {
        this.deductPensionAfterTax = deductPensionAfterTax;
    }

   
    public String getPayrollPrefix() {
        return payrollPrefix;
    }

    public void setPayrollPrefix(String payrollPrefix) {
        this.payrollPrefix = payrollPrefix;
    }    
    
    //Save
    public String save(){
       List<GeneralParameters> parameters = new ArrayList<>();       
        prepareAndSave();
        return "viewGeneralParameters";
    }
    
    public void prepareAndSave(){
    List<String> lists = new ArrayList<>(
     Arrays.asList(
     "PersonalRelief","NSSFFigure","MaximumNSSF","DeductPensionAfterTax","DeductNSSFAfterTax","DeductNHIFAfterTax","NSSFIsPercentage","CalculateNHIFFromBasicSalary",
             "MaxPensionDeductableB4Tax","DeductProvidentAfterTax","CasualRateIsPerDay","HousingPercentageOfGross","HousingPercentage","PayrollPrefix"
             )
   );    
    
      for(String param : lists){
         
         switch(param){
             case "PersonalRelief":
               GeneralParameters parameter1 =  new GeneralParameters();
                parameter1.setParamName("PersonalRelief");
                parameter1.setParamValue(Double.toString(this.personalRelief));               
                this.generalParametersFacade.edit(parameter1);
                break;
            
            case "NSSFFigure":
                 GeneralParameters parameter2 =  new GeneralParameters();
                parameter2.setParamName("NSSFFigure");
                parameter2.setParamValue(Double.toString(this.NSSFFigure));
                this.generalParametersFacade.edit(parameter2);
                break;
                
            case "MaximumNSSF":
                GeneralParameters parameter3 =  new GeneralParameters();
                parameter3.setParamName("MaximumNSSF");
                parameter3.setParamValue(Double.toString(this.maximumNSSF));
                this.generalParametersFacade.edit(parameter3);    
                break;
            
            case "DeductPensionAfterTax":
                 GeneralParameters parameter4 =  new GeneralParameters();
                parameter4.setParamName("DeductPensionAfterTax");
                parameter4.setParamValue(boolToString(this.deductPensionAfterTax));
                this.generalParametersFacade.edit(parameter4);    
                break;
            
            case "DeductNSSFAfterTax":
                GeneralParameters parameter5 =  new GeneralParameters();
                parameter5.setParamName("DeductNSSFAfterTax");
                parameter5.setParamValue(boolToString(this.deductNSSFAfterTax));
                this.generalParametersFacade.edit(parameter5);    
                break;
            
            case "DeductNHIFAfterTax":
                 GeneralParameters parameter6 =  new GeneralParameters();
                parameter6.setParamName("DeductNHIFAfterTax");
                parameter6.setParamValue(boolToString(this.deductNHIFAfterTax));
                this.generalParametersFacade.edit(parameter6);     
                break;
            
            case "NSSFIsPercentage":
                 GeneralParameters parameter7 =  new GeneralParameters();
                parameter7.setParamName("NSSFIsPercentage");
                parameter7.setParamValue(boolToString(this.NSSFIsPercentage));
                this.generalParametersFacade.edit(parameter7);   
                break;
            
            case "CalculateNHIFFromBasicSalary":
                 GeneralParameters parameter8 =  new GeneralParameters();
                parameter8.setParamName("CalculateNHIFFromBasicSalary");
                parameter8.setParamValue(boolToString(this.calculateNHIFFromBasicSalary));
                this.generalParametersFacade.edit(parameter8);     
                break;
                
            case "MaxPensionDeductableB4Tax":
                 GeneralParameters parameter9 =  new GeneralParameters();
                parameter9.setParamName("MaxPensionDeductableB4Tax");
                parameter9.setParamValue(Double.toString(this.maxPensionDeductableB4Tax));
               this.generalParametersFacade.edit(parameter9);  
                break;
            
            case "DeductProvidentAfterTax":
                 GeneralParameters parameter10 =  new GeneralParameters();
                parameter10.setParamName("DeductProvidentAfterTax");
                parameter10.setParamValue(boolToString(this.deductProvidentAfterTax));
                this.generalParametersFacade.edit(parameter10);    
                break;
            
            case "CasualRateIsPerDay":
                 GeneralParameters parameter11 =  new GeneralParameters();
                parameter11.setParamName("CasualRateIsPerDay");
                parameter11.setParamValue(boolToString(this.casualRateIsPerDay));
                this.generalParametersFacade.edit(parameter11);    
                break;
            
            case "HousingPercentageOfGross":
                 GeneralParameters parameter12 =  new GeneralParameters();
                parameter12.setParamName("HousingPercentageOfGross");
                parameter12.setParamValue(boolToString(this.housingPercentageOfGross));
                this.generalParametersFacade.edit(parameter12);     
                break;
            
            case "HousingPercentage":
                GeneralParameters parameter13 =  new GeneralParameters();
                parameter13.setParamName("HousingPercentage");
                parameter13.setParamValue(Double.toString(this.housingPercentage));  
                this.generalParametersFacade.edit(parameter13);
                break;
                
             case "PayrollPrefix":
                  GeneralParameters parameter14 =  new GeneralParameters();
                parameter14.setParamName("PayrollPrefix");
                parameter14.setParamValue(this.payrollPrefix);
                this.generalParametersFacade.edit(parameter14); 
                break;
            
                default :
                    System.err.println("Param Default" + param);
                  break;
                  
                 
         }
     }
    
    // return parametersList;
    }  
    
    public String boolToString(boolean b){
        String value = "YES";
        if(b==false){
            value= "NO";
        }   
        return value;
    }
    
    public boolean StringTobool(String b){
       boolean value = true;
       if("NO".equals(b)){
           value = false;
       }
       return value;
    }
    
    /**
     *
     * @return
     */
    public String getOpenGeneralParams(){
        this.NSSFFigure = Double.parseDouble(generalParametersFacade.find("NSSFFigure").getParamValue());
        this.personalRelief = Double.parseDouble(generalParametersFacade.find("personalRelief").getParamValue());
        this.maximumNSSF = Double.parseDouble(generalParametersFacade.find("maximumNSSF").getParamValue());
        this.deductPensionAfterTax = StringTobool(generalParametersFacade.find("deductPensionAfterTax").getParamValue());
        this.deductNSSFAfterTax= StringTobool(generalParametersFacade.find("deductNSSFAfterTax").getParamValue());
        this.deductNHIFAfterTax= StringTobool(generalParametersFacade.find("deductNHIFAfterTax").getParamValue());
        this.NSSFIsPercentage= StringTobool(generalParametersFacade.find("NSSFIsPercentage").getParamValue());
        this.calculateNHIFFromBasicSalary= StringTobool(generalParametersFacade.find("calculateNHIFFromBasicSalary").getParamValue());
        this.maxPensionDeductableB4Tax = Double.parseDouble(generalParametersFacade.find("maxPensionDeductableB4Tax").getParamValue());
        this.deductProvidentAfterTax= StringTobool(generalParametersFacade.find("deductProvidentAfterTax").getParamValue());
        this.casualRateIsPerDay= StringTobool(generalParametersFacade.find("casualRateIsPerDay").getParamValue());
        this.housingPercentageOfGross= StringTobool(generalParametersFacade.find("housingPercentageOfGross").getParamValue());
        this.housingPercentage = Double.parseDouble(generalParametersFacade.find("housingPercentage").getParamValue());
        this.payrollPrefix = generalParametersFacade.find("payrollPrefix").getParamValue();
        
        return "addGeneralParameter";
    }  
   
}