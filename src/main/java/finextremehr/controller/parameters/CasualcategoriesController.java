/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.parameters;

import finextremehr.entity.parameters.Casualcategories;
import finextremehr.service.CasualcategoriesFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author kadan
 */
@Named(value = "casualcategoriesController")
@SessionScoped
public class CasualcategoriesController implements Serializable {

    @EJB
    private CasualcategoriesFacade casualcategoriesFacade;
    
    private Casualcategories casualcategory = new Casualcategories();

    
    public CasualcategoriesController() {
    }

    public Casualcategories getCasualcategory() {
        return casualcategory;
    }

    public void setCasualcategory(Casualcategories casualcategory) {
        this.casualcategory = casualcategory;
    }
    
      //Display All 
    public List<Casualcategories> findAll(){
        return this.casualcategoriesFacade.findAll();
    }
    
    //Add new award
    public String add(){
        this.casualcategoriesFacade.create(this.casualcategory);
        this.casualcategory = new Casualcategories();
        return "viewCasualcategories";
    }
    //Loading the Edit page
    public String edit(Casualcategories casualcategory){
        this.casualcategory = casualcategory;
        return "editCasualcategory";
    }
    //Updating  Details
    public String update(){
        this.casualcategoriesFacade.edit(this.casualcategory);
        this.casualcategory = new Casualcategories();
        return "viewCasualcategories";
    }
    
    //Delete 
    public void delete(Casualcategories casualcategory){
        this.casualcategoriesFacade.remove(casualcategory);
    } 
}
