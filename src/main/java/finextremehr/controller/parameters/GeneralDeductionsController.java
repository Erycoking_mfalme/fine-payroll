
package finextremehr.controller.parameters;

import finextremehr.entity.parameters.GeneralDeductions;
import finextremehr.enums.YesNo;
import finextremehr.service.GeneralDeductionsFacade;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author kadan
 */
@Named(value = "generalDeductionsController")
@RequestScoped
public class GeneralDeductionsController implements Serializable {

    @EJB
    private GeneralDeductionsFacade generalDeductionsFacade;
    private GeneralDeductions deduction = new GeneralDeductions();
    
    
    private boolean hasBal = true;
    private boolean isDefaultSaccoDeduction;
    private boolean isSaccoSaving;
    private boolean isWelfareFund;
    private boolean doesNotRecur;
    private boolean deductAfterTax;
    private boolean isVoluntaryNSSF;
    private boolean isLoanDeduction = true;
    private boolean isSaccoLoan;
    private boolean otherReliefs = true;
    
    private String OtherReliefsName= "Fixed Amount"; 
    
    private boolean deleteDisabled = false;

    public boolean isDeleteDisabled() {
        return deleteDisabled;
    }

    public void setDeleteDisabled(boolean deleteDisabled) {
        this.deleteDisabled = deleteDisabled;
    }

    
    

    public String getOtherReliefsName() {
        return OtherReliefsName;
    }

    public void setOtherReliefsName(String OtherReliefsName) {
        this.OtherReliefsName = OtherReliefsName;
    }
   
    

    public boolean isOtherReliefs() {
        return otherReliefs;
    }

    public void setOtherReliefs(boolean otherReliefs) {
       
        this.otherReliefs = otherReliefs;
    }
    
   
    public boolean isIsSaccoLoan() {
        return isSaccoLoan;
    }

    public void setIsSaccoLoan(boolean isSaccoLoan) {
         if(isSaccoLoan == false){
             this.deduction.setIsSaccoLoan(YesNo.NO);
        }else{
            this.deduction.setIsSaccoLoan(YesNo.YES);
        }
        this.isSaccoLoan = isSaccoLoan;
    }

    

    public boolean isIsLoanDeduction() {
        
        return isLoanDeduction;
    }

    public void setIsLoanDeduction(boolean isLoanDeduction) {
          if(isLoanDeduction == false){
             this.deduction.setIsLoanDeduction(YesNo.NO);
        }else{
            this.deduction.setIsLoanDeduction(YesNo.YES);
        }
        this.isLoanDeduction = isLoanDeduction;
    }
    
    
    
    
    

    public boolean isIsVoluntaryNSSF() {
        return isVoluntaryNSSF;
    }

    public void setIsVoluntaryNSSF(boolean isVoluntaryNSSF) {
         if(isVoluntaryNSSF == false){
             this.deduction.setIsVoluntaryNSSF(YesNo.NO);
        }else{
            this.deduction.setIsVoluntaryNSSF(YesNo.YES);
        }
        this.isVoluntaryNSSF = isVoluntaryNSSF;
    }
    
    

    public boolean isDeductAfterTax() {
        return deductAfterTax;
    }

    public void setDeductAfterTax(boolean deductAfterTax) {
         if(deductAfterTax == false){
             this.deduction.setDeductAfterTax(YesNo.NO);
        }else{
            this.deduction.setDeductAfterTax(YesNo.YES);
        }
        this.deductAfterTax = deductAfterTax;
    }
    
    

    public boolean isDoesNotRecur() {
        return doesNotRecur;
    }

    public void setDoesNotRecur(boolean doesNotRecur) {
        
         if(doesNotRecur == false){
             this.deduction.setDoesNotRecur(YesNo.NO);
        }else{
            this.deduction.setDoesNotRecur(YesNo.YES);
        }
        this.doesNotRecur = doesNotRecur;
    }
    
    

    public boolean isIsWelfareFund() {
        return isWelfareFund;
    }

    public void setIsWelfareFund(boolean isWelfareFund) {
         if(isWelfareFund == false){
             this.deduction.setIsWelfareFund(YesNo.NO);
        }else{
            this.deduction.setIsWelfareFund(YesNo.YES);
        }
        this.isWelfareFund = isWelfareFund;
    }
    
    

    public boolean isIsSaccoSaving() {
        return isSaccoSaving;
    }

    public void setIsSaccoSaving(boolean isSaccoSaving) {
         if(isSaccoSaving == false){
             this.deduction.setIsSaccoSaving(YesNo.NO);
        }else{
            this.deduction.setIsSaccoSaving(YesNo.YES);
        }
        this.isSaccoSaving = isSaccoSaving;
    }
    
    

    public boolean isIsDefaultSaccoDeduction() {
        return isDefaultSaccoDeduction;
    }

    public void setIsDefaultSaccoDeduction(boolean isDefaultSaccoDeduction) {
         if(isDefaultSaccoDeduction == false){
             this.deduction.setIsDefaultSaccoDeduction(YesNo.NO);
        }else{
            this.deduction.setIsDefaultSaccoDeduction(YesNo.YES);
        }
        this.isDefaultSaccoDeduction = isDefaultSaccoDeduction;
    }
    
    

    public boolean isHasBal() {
        return hasBal;
    }

    public void setHasBal(boolean hasBal) {
         if(hasBal == false){
             this.deduction.setHasBal(YesNo.NO);
        }else{
            this.deduction.setHasBal(YesNo.YES);
        }
        this.hasBal = hasBal;
    }
    
    

    public GeneralDeductionsController() {
    }

    public GeneralDeductions getDeduction() {
        return deduction;
    }

    public void setDeduction(GeneralDeductions deduction) {
        this.deduction = deduction;
    }
    
     //Display All 
    public List<GeneralDeductions> findAll(){
        deleteDisabled = true;
//        this.deduction = generalDeductionsFacade.findAll().get(0);
        return this.generalDeductionsFacade.findAll();
    }
    
    //Add new/Edit award
    public String add(){

        if(this.isLoanDeduction == false){
           this.deduction.setIsSaccoLoan(YesNo.NO);
        }

        this.generalDeductionsFacade.edit(this.deduction);
        this.deduction = new GeneralDeductions();
        return "viewDeductions";
    }
       

    public void delete2(){        
        this.generalDeductionsFacade.remove(this.deduction); 
        this.deduction = new GeneralDeductions();
    }
    public void loadDeduction(){
        try {
            //Convert the enums to boolean  
       
        this.isLoanDeduction = deduction.getIsLoanDeduction() == YesNo.YES;
        
        this.hasBal = deduction.getHasBal()== YesNo.YES;
        
        this.isDefaultSaccoDeduction = deduction.getIsDefaultSaccoDeduction()== YesNo.YES;
        
        this.isSaccoSaving = deduction.getIsSaccoSaving()== YesNo.YES;
        
        this.isWelfareFund = deduction.getIsWelfareFund()== YesNo.YES;
        
        this.doesNotRecur = deduction.getDoesNotRecur()== YesNo.YES;
        
        this.deductAfterTax = deduction.getDeductAfterTax()== YesNo.YES;
        
        this.isVoluntaryNSSF = deduction.getIsVoluntaryNSSF()== YesNo.YES;
        
        this.isSaccoLoan = deduction.getIsSaccoLoan()== YesNo.YES;
                
        this.isWelfareFund = deduction.getIsWelfareFund()== YesNo.YES;
           
        this.otherReliefs = deduction.getReliefName() != null; 
        
        } catch (Exception e) {
            System.err.println("Erro while fetching Deduction");
             System.err.println(e);
        }
        
        
    }
    
    public void SetOtherReliefsName(){
        if(this.deduction.getReliefIsPercentage() == YesNo.YES){
            OtherReliefsName= "Percentage";
        }else{
            OtherReliefsName= "Fixed Amount";
        }
    }
     
   
}
