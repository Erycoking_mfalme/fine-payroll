/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.parameters;

import finextremehr.entity.parameters.PAYEBrackets;
import finextremehr.service.PAYEBracketsFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author kadan
 */
@Named(value = "pAYEBracketsController")
@SessionScoped
public class PAYEBracketsController implements Serializable {

    @EJB
    private PAYEBracketsFacade pAYEBracketsFacade;
    private PAYEBrackets pAYEBracket = new PAYEBrackets();
   
    public PAYEBracketsController() {
    }

    public PAYEBrackets getpAYEBracket() {
        return pAYEBracket;
    }

    public void setpAYEBracket(PAYEBrackets pAYEBracket) {
        this.pAYEBracket = pAYEBracket;
    }    
    
     //Display All 
    public List<PAYEBrackets> findAll(){
        return this.pAYEBracketsFacade.findAll();
    }
    
    //Add new award
    public String add(){
        this.pAYEBracketsFacade.create(this.pAYEBracket);
        this.pAYEBracket = new PAYEBrackets();
        return "viewPAYEBrackets";
    }
    //Loading the Edit page
    public String edit(PAYEBrackets pAYEBracket){
        this.pAYEBracket = pAYEBracket;
        return "editPAYEBracket";
    }
    //Updating  Details
    public String update(){
        this.pAYEBracketsFacade.edit(this.pAYEBracket);
        this.pAYEBracket = new PAYEBrackets();
        return "viewPAYEBrackets";
    }
    
    //Delete 
    public void delete(PAYEBrackets pAYEBracket){
        this.pAYEBracketsFacade.remove(pAYEBracket);
    } 
}
