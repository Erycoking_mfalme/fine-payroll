/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.parameters;

import finextremehr.entity.parameters.GeneralEarnings;
import finextremehr.enums.YesNo;
import finextremehr.service.GeneralEarningsFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author kadan
 */
@Named(value = "generalEarningsController")
@RequestScoped
public class GeneralEarningsController implements Serializable {

    @EJB
    private GeneralEarningsFacade generalEarningsFacade;
    private GeneralEarnings genEarning = new GeneralEarnings();
    
    private boolean taxable = true;
    private boolean considerNHIF;
//    private boolean isHousing;
//    private boolean isPercOfBasic;
    private boolean isBasicSalary;
    private boolean doesNotRecur;

    
    public GeneralEarningsController() {
    }

    public boolean isTaxable() {
        return taxable;
    }

    public void setTaxable(boolean taxable) {
        if (taxable == false) {
            this.genEarning.setTaxable(YesNo.NO);
        } else {
            this.genEarning.setTaxable(YesNo.YES);
        }
        this.taxable = taxable;
    }

    public boolean isConsiderNHIF() {
        return considerNHIF;
    }

    public void setConsiderNHIF(boolean considerNHIF) {
        if (considerNHIF == false) {
            this.genEarning.setConsiderNHIF(YesNo.NO);
        } else {
            this.genEarning.setConsiderNHIF(YesNo.YES);
        }
        this.considerNHIF = considerNHIF;
    }

    public boolean isIsBasicSalary() {
        return isBasicSalary;
    }

    public void setIsBasicSalary(boolean isBasicSalary) {
        if (isBasicSalary == false) {
            this.genEarning.setIsBasicSalary(YesNo.NO);
        } else {
            this.genEarning.setIsBasicSalary(YesNo.YES);
        }
        this.isBasicSalary = isBasicSalary;
    }

    public boolean isDoesNotRecur() {
        return doesNotRecur;
    }

    public void setDoesNotRecur(boolean doesNotRecur) {
        if (doesNotRecur == false) {
            this.genEarning.setDoesNotRecur(YesNo.NO);
        } else {
            this.genEarning.setDoesNotRecur(YesNo.YES);
        }
        this.doesNotRecur = doesNotRecur;
    }

    
    public GeneralEarnings getGenEarning() {
        return genEarning;
    }

    public void setGenEarning(GeneralEarnings genEarning) {
        this.genEarning = genEarning;
    }
    
     //Display All 
    public List<GeneralEarnings> findAll(){
        return this.generalEarningsFacade.findAll();
    }
    
    //Add new/edit award
    public void add(){
        this.generalEarningsFacade.edit(this.genEarning);
        this.genEarning = new GeneralEarnings();
    }
    
    //ClearDetails
    public void Clearpage(){        
        this.genEarning = new GeneralEarnings();       
    }
    
    //Delete 
    public void delete(){
        this.generalEarningsFacade.remove(genEarning); 
        this.genEarning = new GeneralEarnings();
    } 
    
    public void loadEarnings(){       
            //Convert the enums to boolean        
        this.considerNHIF = genEarning.getConsiderNHIF()== YesNo.YES;
        
        this.taxable = genEarning.getTaxable()== YesNo.YES;
        
        this.isBasicSalary = genEarning.getIsBasicSalary()== YesNo.YES;
        
        this.doesNotRecur = genEarning.getDoesNotRecur()== YesNo.YES;        
    }
}
