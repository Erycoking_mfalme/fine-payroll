/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.parameters;

import finextremehr.entity.parameters.TypesOfLeave;
import finextremehr.service.TypesOfLeaveFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author fred
 */
@Named(value = "leaveTypesController")
@SessionScoped
public class LeaveTypesController implements Serializable {

    @EJB
    private TypesOfLeaveFacade typesOfLeaveFacade;
    
    
    private TypesOfLeave leaveType;

    public TypesOfLeave getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(TypesOfLeave leaveType) {
        this.leaveType = leaveType;
    }
    
    

    /**
     * Creates a new instance of LeaveTypesController
     */
    public LeaveTypesController() {
    }
    
    //Display All Leave Types
    public List<TypesOfLeave> findAll() {        
        return this.typesOfLeaveFacade.findAll();
    }

     
    //Add new Bank
    public String add() {

       this.typesOfLeaveFacade.create(this.leaveType);
       this.leaveType = new TypesOfLeave();
        return "viewLeaveTypes";
    }

    //Loading Leave Type to the Edit page
    public String edit(TypesOfLeave leav) {
        this.leaveType = leav;
        return "editLeaveType";
    }

   

    //Updating Bank Details
    public String update() {
        this.typesOfLeaveFacade.edit(this.leaveType);
       this.leaveType = new TypesOfLeave();
        return "viewLeaveTypes";
    }

    //Delete Bank
    public void delete(TypesOfLeave leave) {
        this.typesOfLeaveFacade.remove(leave);

    }

}
