/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.parameters;

import finextremehr.entity.parameters.NonWorkingDays;
import finextremehr.service.NonWorkingDaysFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.SimpleDateFormat;


import java.util.List;



import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;






/**
 *
 * @author fred
 */
@Named(value = "nonWorkingDaysController")
@RequestScoped
public class NonWorkingDaysController implements Serializable {   

    @EJB
    private NonWorkingDaysFacade nonWorkingDaysFacade; 
    
   
    
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
   
   private NonWorkingDays nonWorkingDays = new NonWorkingDays();
   private Boolean disabled = true;

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }
   
 
   public void onDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
      
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
    }
     
    

    public NonWorkingDays getNonWorkingDays() {
        return nonWorkingDays;
    }

    public void setNonWorkingDays(NonWorkingDays nonWorkingDays) {
        this.nonWorkingDays = nonWorkingDays;
    }

    public void save(){
        System.err.println("this.nonWorkingDays: " + this.nonWorkingDays);
        this.nonWorkingDaysFacade.edit(this.nonWorkingDays);
        this.nonWorkingDays = new NonWorkingDays();
       
    }
    
         //Display All 
    public List<NonWorkingDays> findAll(){
        return this.nonWorkingDaysFacade.findAll();
    }
    
    //Add new award

    public void add(){       
        this.nonWorkingDays = new NonWorkingDays();        
    }
    //Loading the Edit page
    public void edit(NonWorkingDays nonWorkingDays){
        this.nonWorkingDays = nonWorkingDays; 
    }
    //Updating  Details
    public String update(){
        this.nonWorkingDaysFacade.edit(this.nonWorkingDays);
        this.nonWorkingDays = new NonWorkingDays();
        return "nonWorkingDays";
    }
    
    //Delete 
    public void delete(NonWorkingDays nonWorkingDays){
        this.nonWorkingDaysFacade.remove(nonWorkingDays);        
    } 
   
    public void onRowSelect(SelectEvent event) {
        System.err.println("Disable false");
        disabled = false;
    }  
    public void onRowUnSelect(SelectEvent event) {
        System.err.println("Disable True");
        disabled = true;
    } 

    

}
