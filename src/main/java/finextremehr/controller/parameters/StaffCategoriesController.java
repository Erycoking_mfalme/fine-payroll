/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package finextremehr.controller.parameters;

import finextremehr.entity.parameters.StaffCategories;
import finextremehr.enums.YesNo;
import finextremehr.service.StaffCategoriesFacade;
import javax.inject.Named;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;


/**
 *
 * @author kadan
 */
@Named(value = "staffCategoriesController")
@RequestScoped

public class StaffCategoriesController implements Serializable {

    @EJB
    private StaffCategoriesFacade staffCategoriesFacade;
    
    private StaffCategories staffCategory = new StaffCategories();
    
    private boolean hasRelief;
    private boolean autoHousing;
    private boolean excemptedFromPaye;

    public boolean isHasRelief() {
        return hasRelief;
    }

    public void setHasRelief(boolean hasRelief) {
        if(hasRelief == false){
             this.staffCategory.setHasRelief(YesNo.NO);
        }else{
            this.staffCategory.setHasRelief(YesNo.YES);
        }
        
        this.hasRelief = hasRelief;
    }

    public boolean isAutoHousing() {
        
        
        return autoHousing;
    }

    public void setAutoHousing(boolean autoHousing) {
        
         if(autoHousing == false){
             this.staffCategory.setAutoHousing(YesNo.NO);
        }else{
            this.staffCategory.setAutoHousing(YesNo.YES);
        }
        this.autoHousing = autoHousing;
    }

    public boolean isExcemptedFromPaye() {
        
        return excemptedFromPaye;
    }

    public void setExcemptedFromPaye(boolean excemptedFromPaye) {
         if(excemptedFromPaye == false){
             this.staffCategory.setExcemptedFromPaye(YesNo.NO);
        }else{
            this.staffCategory.setExcemptedFromPaye(YesNo.YES);
        }
        this.excemptedFromPaye = excemptedFromPaye;
    }
    
    
    
    public StaffCategoriesController() {
    }

    public StaffCategories getStaffCategory() {
        return staffCategory;
    }

    public void setStaffCategory(StaffCategories staffCategory) {
        this.staffCategory = staffCategory;
    }

    
    
    
     //Display All 
    public List<StaffCategories> findAll(){
        return this.staffCategoriesFacade.findAll();
    }
    
    //Add new award
    public String add(){
       
        this.staffCategoriesFacade.edit(this.staffCategory);
        this.staffCategory = new StaffCategories();
        return "viewStaffCategories";
    }
    //Loading the Edit page
    public String edit(StaffCategories staffCategory){
        
        //Convert the enums to boolean
        this.autoHousing = staffCategory.getAutoHousing() == YesNo.YES;
        
        this.hasRelief = staffCategory.getHasRelief()== YesNo.YES;
        
        this.excemptedFromPaye = staffCategory.getExcemptedFromPaye() == YesNo.YES;
        
        this.staffCategory = staffCategory;
        return "editStaffCategory";
    }
       //Delete 
    public void delete(StaffCategories staffCategory){
        this.staffCategoriesFacade.remove(staffCategory);
    } 
    
}
